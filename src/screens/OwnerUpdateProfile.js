import React, { Component } from 'react';
import {
    View, TouchableOpacity, FlatList, NetInfo, Keyboard, Platform, ScrollView, 
    AsyncStorage, Dimensions
} from 'react-native';
import { connect } from 'react-redux';
// import Icon from 'react-native-vector-icons/EvilIcons';
import { Item, Label, Picker, Button, Icon } from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import ChangeableHeader from '../components/ChangeableHeader';
import Strings from '../assets/strings';
import AppSpinner from '../common/AppSpinner';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import Checkbox from 'react-native-custom-checkbox';
import { signup, goToNext, signupStep4 } from '../actions/SignupAction';
import MapView, { Marker } from 'react-native-maps';
import Permissions from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box"
import { RNToasty } from 'react-native-toasty';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import {CheckLoginWhenOpen} from '../actions/AuthActions';
import {goToRoot} from '../actions/SignupAction';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import SnackBar from 'react-native-snackbar-component'
import strings from '../assets/strings';
import * as Animatable from "react-native-animatable";
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);



let companies = [];
let location = [];

const { height, width } = Dimensions.get('window');

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }

    if (!values.firstName) {
        errors.firstName = Strings.require;
    } else if (!isNaN(values.firstName)) {
        errors.firstName = Strings.errorName;
    }

    if (!values.lastName) {
        errors.lastName = Strings.require;
    } else if (!isNaN(values.lastName)) {
        errors.lastName = Strings.errorName;
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = Strings.require;
    } else if (!values.phoneNumber.startsWith('05')) {
        errors.phoneNumber = Strings.errorStartPhone;
    } else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = Strings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = Strings.errorPhoneFormat
    }

    return errors;
};

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef, returnKeyType, onSubmit, onChange, input, label, borderColor,
            type, password, numeric, textColor, icon, iconType, marginBottom, email,
            isRTL, iconColor, editable, isRequired, meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class OwnerUpdateProfile extends Component {

    companies=[];

    state = {
        citiesLoadig: true,
        cities: [],
        selectedCity: this.props.currentUser.user.city.id,
        areaLoading: true,
        areas: [],
        selectedArea: this.props.currentUser.user.area.id,
        noConnection:null,
        companies: [],
        companyLoading: true,
        user: {},
        latMap: this.props.currentUser.user.location.length>0?  this.props.currentUser.user.location[0]:0,
        lngMap: this.props.currentUser.user.location.length>0?  this.props.currentUser.user.location[1]:0,
        lat: this.props.currentUser.user.location.length>0?  this.props.currentUser.user.location[0]:0,
        lng: this.props.currentUser.user.location.length>0?  this.props.currentUser.user.location[1]:0,
        latLocation: 0,
        lngLocation: 0,
        updateLoading:false,
        noConnection:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator;
        this.props.currentUser.user.company.filter(company=>{
            console.log(company)
            console.log(company.id)
            this.companies.push(company.id)
        });
        console.log("COMPS")
        console.log(this.companies)
        console.log(typeof(this.companies[0]))
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getCities();
                this.getCompanies();
            }else{
                this.setState({noConnection:Strings.noConnection})
            }
          });
    }

    getCities = () => {
        axios.get(`${BASE_END_POINT}cities`)
            .then(response => {
                this.setState({
                    citiesLoadig: false,
                    cities: response.data,
                }, () => this.getAreas(response.data[0].id))
            }).catch(error => {
                if (!error.response) {
                    this.setState({ noConnection: Strings.noConnection})
                }
            });
    }

    getAreas = (cityID) => {
        this.setState({ areas: [], areaLoading: true })
        axios.get(`${BASE_END_POINT}cities/${cityID}/areas`)
            .then(response => {
                this.setState({
                    areaLoading: false,
                    areas: response.data,
                   // selectedArea: response.data[0].id
                })
            }).catch(error => {
                console.log(error.response);
                if (!error.response) {
                    this.setState({ snoConnection: Strigs.noConnection, areaLoading: false })
                }
            });
    }

    ////////////////////////

    getCompanies = () => {
        axios.get(`${BASE_END_POINT}companies`)
            .then(response => {
                this.setState({
                    companies: response.data.data,
                    companyLoading: false
                })
            }).catch(error => {
                console.log(error.respose);
            })
    }

    componentDidMount() {
        this.disableDrawer();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})
                    this.getCities();
                    this.getCompanies();
                }
            }
          ); 
    }

    _requestPermission = () => {
        Permissions.request('location').then(response => {
            if (response === 'denied' || response === 'undetermined' || response === 'denied') {
                this._requestPermission()
            } else {
                this.getLatLng();
            }
        })
    }

    permissions = () => {
        Permissions.check('location').then(response => {
            if (response === 'denied' || response === 'undetermined' || response === 'denied') {
                this._requestPermission()
            } else {
                this.getLatLng();
            }
        });
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    getLatLng() {
        Geolocation.getCurrentPosition(
            (position) => {
                console.log("position => " + position.coords);
                this.setState({
                    latLocation: position.coords.latitude,
                    lngLocation: position.coords.longitude,
                    error: null,
                });

            },
            (error) => {
                this.dialogGPS();
            },
            { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000 }
        );

    }


    dialogGPS = () => {
        if (Platform.OS === 'android') {
            LocationServicesDialogBox.checkLocationServicesIsEnabled({
                message: Strings.pleaseOpenGPS,
                ok: Strings.yes,
                cancel: Strings.no,
                enableHighAccuracy: true,
                showDialog: true,
                openLocationServices: true,
                preventOutSideTouch: true,
                preventBackClick: true
            }).then(() => {
                this.getLatLng();
            }).catch(error => RNToasty.Error({ title: allStrings.cannotDetermineAddress }));
        }
        else {
            this.getLatLng();

        }
    }

    leftComponentOfStep3 = () => (
        <MyTouchableOpacity onPress={() => {
            if (this.state.lat&&this.state.lng) {
                const user = {
                    ...this.state.user,
                    location: [this.state.lat, this.state.lng]
                }
                this.setState({ user: user })
                console.log(user);
                this.props.goToNext();
            } else {
                RNToasty.Warn({title:Strings.chooseLocation})
                // this.setState({ errorText: Strings.selectCompany, showSnack: true });
            }
        }}>
            <Icon name={"check"} type="Entypo" style={{ color: 'white' }} size={18} />
        </MyTouchableOpacity>

    )


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }


    renderCityPicker = () => {
        return (
            <Item style={{ borderBottomColor: 'gray', marginTop: moderateScale(7), marginBottom: moderateScale(5), width: responsiveWidth(80), borderWidth: 2, flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
                <View style={{ flex: 1 }}>
                    <Label style={{ fontSize: responsiveFontSize(3), color: '#73231F20', alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start' }}>{Strings.city}</Label>
                </View>
                <View style={{ flex: 1 }}>
                    {
                        this.state.citiesLoadig ?
                            <AppSpinner />
                            :
                            <Picker
                                mode="dropdown"
                                iosHeader="Select your SIM"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                                selectedValue={this.state.selectedCity}
                                style={{ alignSelf: this.props.isRTL ? 'flex-start' : 'flex-end', height: responsiveHeight(2), borderWidth: 2, width: responsiveWidth(35), }}
                                onValueChange={(value, index) => {
                                    this.setState({ selectedCity: value });
                                    this.getAreas(value);
                                }}
                            >
                                {this.state.cities.map(obj => (
                                    <Picker.Item key={obj.id} label={obj.cityName} value={obj.id} />
                                ))}

                            </Picker>
                    }
                </View>
            </Item>
        )
    }

    renderAreaPicker = () => {
        console.log("rtl  " + this.props.isRTL);
        return (
            <Item style={{ borderBottomColor: 'gray', marginTop: moderateScale(7), marginBottom: moderateScale(5), width: responsiveWidth(80), borderWidth: 2, flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
                <View style={{ flex: 1 }}>
                    <Label style={{ fontSize: responsiveFontSize(3), color: '#73231F20', alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start' }}>{Strings.area}</Label>
                </View>
                <View style={{ flex: 1 }}>
                    {
                        this.state.areaLoading ?
                            <AppSpinner isRTL={this.props.isRTL} />
                            :
                            <Picker
                                mode="dropdown"
                                iosHeader="Select your SIM"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                                selectedValue={this.state.selectedArea}
                                style={{ alignSelf: this.props.isRTL ? 'flex-start' : 'flex-end', height: responsiveHeight(2), borderWidth: 2, width: responsiveWidth(35), }}
                                onValueChange={(value, index) => {
                                    this.setState({ selectedArea: value });
                                }}
                            >
                                {this.state.areas.map(obj => (
                                    <Picker.Item key={obj.id} label={obj.areaName} value={obj.id} />
                                ))}
                            </Picker>
                    }
                </View>
            </Item>
        )
    }

    renderContent() {
        // const { navigator, isRTL } = this.props;
        return (
            <View>

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} name="firstName" isRTL={this.props.isRTL} marginBottom={moderateScale(3)} label={Strings.firstName} component={InputComponent} 
                    returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss()
                    }}
                />
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} name="lastName" isRTL={this.props.isRTL} marginBottom={moderateScale(3)} label={Strings.lastName} component={InputComponent} 
                returnKeyType="done"
                onSubmit={() => {
                    Keyboard.dismiss()
                }}
                />
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} email name="email" isRTL={this.props.isRTL} marginBottom={moderateScale(3)} label={Strings.email} component={InputComponent} 
                  returnKeyType="done"
                  onSubmit={() => {
                      Keyboard.dismiss()
                  }}
                />

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} numeric name="phoneNumber" isRTL={this.props.isRTL} marginBottom={moderateScale(3)} label={Strings.phoneNumer} component={InputComponent} 
                  returnKeyType="done"
                  onSubmit={() => {
                      Keyboard.dismiss()
                  }}
                />

                {this.renderCityPicker()}
                {this.renderAreaPicker()}

            </View>

        )
    }

    leftComponent = () => (
        <MyTouchableOpacity
            onPress={() => {
                if (this.companies.length > 0) {
                    const user = {
                        ...this.state.user,
                        company: this.companies,
                    }
                    this.setState({ user: user })
                    console.log(user);
                    this.props.goToNext();
                } else {
                    this.setState({ errorText: Strings.selectCompany, showSnack: true });
                    //RNToasty.Warn({ title: Strings.selectCompany });
                }
            }}
        >
            <Icon name={"check"} type="Entypo" style={{ color: 'white' }} size={18} />
        </MyTouchableOpacity>
    )

    onUpdateStep1(values) {
        const user = {
            firstname: values.firstName,
            lastname: values.lastName,
            email: values.email,
            phone: values.phoneNumber,
            city: this.state.selectedCity,
            area: this.state.selectedArea,
            //type: this.props.clientType,
            //token: this.props.userToken,
        }
        this.setState({ user: user });
        console.log(user);
        this.props.goToNext();
    }

    onUpload() {
        console.log('values');
        console.log(this.state.user);
        this.setState({updateLoading:true})
        axios.put(`${BASE_END_POINT}user/updateInfo`, JSON.stringify(this.state.user), {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
            console.log('user')    
            console.log(response.data)
            this.setState({updateLoading:false,noConnection:null});           
            const user = {...this.props.currentUser,user:{...response.data.user}}
            console.log('current user')
            console.log(user)
            this.props.CheckLoginWhenOpen(user)
            console.log('current user')
            console.log(user)
            AsyncStorage.setItem('@BoodyCarUser', JSON.stringify(user));
            this.props.goToRoot();
            this.props.navigator.resetTo({
                screen: 'OwnerHome',
                animated: true,
            })
        }).catch(error => {
            console.log('error');
            this.setState({updateLoading:false});
              console.log(error.response);
            if (!error.response) {
            this.setState({noConnection:Strings.noConnection});
            } 
          });
    }
    
    renderSaveButton = () => (
        <View style={{ width: responsiveWidth(100) }}>
            <Button
                onPress={() => {
                    this.onUpload()
                    //this.props.signup(this.state.user, this.props.navigator);
                }}
                style={{alignSelf:'center', marginTop: moderateScale(10), marginBottom: moderateScale(10), justifyContent: 'center', alignItems: 'center', height: responsiveHeight(8), width: responsiveWidth(80), backgroundColor: colors.primaryColor, borderRadius: moderateScale(2.5) }}>
                <AppText text={Strings.save} color='white' fontSize={responsiveFontSize(3)} />
            </Button>
        </View>
    )

    renderstep4 = () => (
        <View style={{ flex: 1 }}>
            <ChangeableHeader title={Strings.updateProfile} showBack />
            <View style={{ width: responsiveWidth(100), justifyContent: 'center', alignItems: 'center' }}>
                {this.renderSaveButton()}
            </View>
            {this.state.updateLoading && <LoadingDialogOverlay title={Strings.updateProfile}/>}
            <SnackBar
                visible={this.state.noConnection!=null?true:false}
                textMessage={this.state.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
            />
        </View>
    )

    renderStep1 = () => (
        <View style={{ flex: 1 }}>
            <AppHeader navigator={this.props.navigator} title={Strings.updateProfile} showBack />
            <ScrollView style={{flex: 1 }} >
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, marginTop: moderateScale(5), width: responsiveWidth(100) }}>
                    <View style={{ width: responsiveWidth(80) }}>
                        {this.renderContent()}
                    </View>
                </View>
            </ScrollView>
            <View style={{width:responsiveWidth(100),position:'absolute',bottom:0}}>
                <MyTouchableOpacity
                    onPress={this.props.handleSubmit(this.onUpdateStep1.bind(this))}
                    style={{ margin: moderateScale(6), marginHorizontal:moderateScale(8), alignSelf: this.props.isRTL ? 'flex-start' : 'flex-end', flexDirection: this.props.isRTL ? 'row-reverse' : 'row'}}>
                    <AppText color={colors.skipIconColor} text={Strings.next} fontSize={responsiveFontSize(3.5)} />
                    <Icon name={this.props.isRTL ? "chevron-left" : "chevron-right"} type="Entypo" style={{ color: colors.skipIconColor }} size={20} />
                </MyTouchableOpacity>
            </View>
            
            {/* for city and area */}
            <SnackBar
                visible={this.state.noConnection!=null?true:false}
                textMessage={this.state.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
            />
        </View>
    )

    renderStep2 = () => (
        <View style={{ flex: 1 }}>
            <ChangeableHeader leftComponent={this.leftComponent()} title={Strings.selectcompany} showBack />
            {
                this.state.companyLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ marginBottom: moderateScale(2) }}>
                            <AppSpinner />
                        </View>
                        <AppText text={Strings.wait} fontSize={responsiveFontSize(3)} color={colors.primaryColor} />
                    </View>
                    :
                    <FlatList
                        style={{ flex: 1 }}
                        data={this.state.companies}
                        keyExtractor={(item, index) => item.id.toString()}
                        renderItem={({ item, index }) => (
                            //flexDirection: this.props.isRTL? 'row-reverse':'row'
                            <Item style={{ alignItems: 'center', height: responsiveHeight(7), marginTop: moderateScale(5), }}>
                                {/*alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'*/}
                                <View style={{ marginHorizontal: moderateScale(8), }}>
                                    <Checkbox
                                        checked={this.companies.includes(item.id) && true}
                                        style={{ color: colors.primaryColor, borderRadius: 5 }}
                                        onChange={(name, checked) => {
                                            if (this.companies.includes(item.id)) {
                                                this.companies = this.companies.filter(val => (
                                                    val != item.id
                                                ))
                                            } else {
                                                this.companies.push(item.id);
                                            }
                                            console.log(this.companies);
                                        }}
                                    />
                                </View>
                                {/* alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start' */}
                                <View style={{ marginHorizontal: moderateScale(-3), }}>
                                    <AppText text={item.companyname} fontSize={18} color={colors.primaryColor} />
                                </View>

                            </Item>
                        )} />
            }

            <SnackBar
                visible={this.state.noConnection!=null?true:false}
                textMessage={this.state.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
            />
        </View>

    )

    renderStep3 = () => {
        this.permissions()
        return (
            <View style={{ flex: 1 }}>
                <ChangeableHeader leftComponent={this.leftComponentOfStep3()} title={Strings.chooseLocation} showBack />
                <View style={{ flex: 1 }} >
                    <Animatable.View ref={ref => this.view = ref} style={{ height: responsiveHeight(5) }} animation="slideInDown" duration={1000} delay={1000} useNativeDriver={true} easing="ease-in-out-quart">
                        <View style={{ flexDirection: !this.props.isRTL ? 'row' : 'row-reverse',paddingHorizontal:moderateScale(5), alignItems: 'center', backgroundColor: "#004f6f",opacity:.9,flex:1}} >
                            <AppText text={strings.longPressToLocation} color="white"   />    
                        </View>
                    </Animatable.View>
                    <MapView
                        ref={ref => { this.mapRef = ref; }}
                        // style={}
                        showsMyLocationButton={true}
                        zoomControlEnabled={true}
                        onLongPress={e => {
                            // this.props.onMapLongPress(e.nativeEvent.coordinate.latitude, e.nativeEvent.coordinate.longitude)
                            this.setState({ lat: e.nativeEvent.coordinate.latitude, lng: e.nativeEvent.coordinate.longitude }
                                , )
                        }}
                        style={{ width: '100%', height: height }}
                        initialRegion={{
                            latitude: this.state.lat,
                            longitude: this.state.lat,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    >
                        <Marker pinColor='red' coordinate={{
                            latitude: this.state.lat,
                            longitude: this.state.lng,
                        }} />
                    </MapView>
                    <MyButton
                        onPress={() => {
                            this.mapRef.animateToRegion({
                                latitude: this.state.latLocation, //e.nativeEvent.coordinate.latitude,
                                longitude: this.state.lngLocation, //e.nativeEvent.coordinate.longitude,
                                latitudeDelta: 0.0059397161733585335,
                                longitudeDelta: 0.005845874547958374
                            })
                            this.setState({lat:this.state.latLocation,lng:this.state.lngLocation})
                        }}
                        style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row', height: responsiveHeight(8), position: 'absolute', bottom: responsiveHeight(3), backgroundColor: colors.primaryColor, borderRadius: moderateScale(3), zIndex: 5000, width: responsiveWidth(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='my-location' type="MaterialIcons" size={responsiveFontSize(3)} color='white' style={{ marginHorizontal: moderateScale(10) }} />
                        <AppText text={Strings.determineLocation} color='white' fontSize={responsiveFontSize(3)} />
                    </MyButton>
                </View>

                <SnackBar
                visible={this.state.noConnection!=null?true:false}
                textMessage={this.state.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
            />
            </View>
        )
    }

    render() {
        if (this.props.step == 1) {
            return this.renderStep1()
        } else if (this.props.step == 2) {
            return this.renderStep2()
        } else if (this.props.step == 3) {
            return this.renderStep3()
        } else if (this.props.step == 4) {
            return this.renderstep4()
        }

    }
}

const form = reduxForm({
    form: "OwnerUpdateProfile",
    enableReinitialize:true,
    validate,
})(OwnerUpdateProfile)

const mapDispatchToProps = {
    signup,
    goToNext,
    CheckLoginWhenOpen,
    goToRoot
}

const mapToStateProps = state => {
    return {
        isRTL: state.lang.RTL,
        signupLoading: state.signup.signupLoading,
        clientType: state.signup.clientType,
        step: state.signup.step,
        userToken: state.auth.userToken,
        currentUser: state.auth.currentUser,
        initialValues: {
            firstName: state.auth.currentUser.user.firstname,
            lastName: state.auth.currentUser.user.lastname,
            email: state.auth.currentUser.user.email,
            phoneNumber: state.auth.currentUser.user.phone,
          },
    }
}


export default connect(mapToStateProps, mapDispatchToProps)(form);

