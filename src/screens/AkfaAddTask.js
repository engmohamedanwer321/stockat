import React, { Component } from 'react';
import {
  View,Keyboard
} from 'react-native';
import {Button} from 'native-base';
import { connect } from 'react-redux';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import AppHeader from '../common/AppHeader';
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import { Field, reduxForm, change as changeFieldValue } from "redux-form";
import AppInput from '../common/AppInput';
import DatePicker from 'react-native-datepicker'


const validate = values => {
    const errors = {};

    const date = values.date
    const incomingNumber = values.incomingNumber;
    const notes = values.notes;

    if (date == null) {
        errors.date = Strings.require;
    }
    if (incomingNumber == null) {
        errors.incomingNumber = Strings.require;
    }
    if (notes == null) {
        errors.notes = Strings.require;
    }

    return errors;
};

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,hiddenUnderLine,noFloating,
            labelColor,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                labelColor = {labelColor}
                noFloating={noFloating}
                hiddenUnderLine={hiddenUnderLine}
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}


class AkfaAddTask extends Component{

    
    
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#06134C',
    };
    state = {
        taskBordercolor:null,
        incomingNumberBorderColor:null,
        notesborderColor:null,
        date:null,
        expectedDate:null
    }
    renderTaskFiled = () => {
        return (
            <View>
                <Field  style={{width:responsiveWidth(50) }} hiddenUnderLine={true} noFloating={true} textColor='black' name="date" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.addTask} component={InputComponent}
                    returnKeyType="done"
                    labelColor={this.state.taskBordercolor?'#00397A':'#A6A6A6'}
                    onFocus={()=>{this.setState({taskBordercolor:'#FF9900'})}}
                    onBlur={()=>{this.setState({taskBordercolor:null})}}
                    onSubmit={() => {
                       Keyboard.dismiss()
                    }}
                />
            </View>
        )        
    }

    renderIncomingNumberFiled = () => {
        return (
            <View>
                <Field  style={{width:responsiveWidth(50) }} hiddenUnderLine={true} noFloating={true} textColor='black' name="incomingDate" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.incomingnumber} component={InputComponent}
                    returnKeyType="done"
                    labelColor={this.state.incomingNumberBorderColor?'#00397A':'#A6A6A6'}
                    onFocus={()=>{this.setState({incomingNumberBorderColor:'#FF9900'})}}
                    onBlur={()=>{this.setState({incomingNumberBorderColor:null})}}
                    onSubmit={() => {
                       Keyboard.dismiss()
                    }}
                />
            </View>
        )        
    }

    renderDate = () => (
        <DatePicker
            style={{width: responsiveWidth(60)}}
            date={this.state.date}
            mode="date"
            placeholder="select date"
            format="YYYY-MM-DD"
            minDate="2016-05-01"
            maxDate="2016-06-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
            dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
            },
            dateInput: {
                marginLeft: 36
            }
            // ... You can check the source to find the other keys.
            }}
            onDateChange={(date) => {this.setState({date: date})}}
      />

    )

    renderExpectedDate = () => (
        <DatePicker
            style={{width: responsiveWidth(50),backgroundColor:'red'}}
            date={this.state.expectedDate}
            mode="date"
            placeholder="select date"
            format="YYYY-MM-DD"
            minDate={`${new Date().getMonth()+1}/${new Date().getDate()}/${new Date().getFullYear()}`}
            maxDate="2016-06-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
            dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
            },
            dateInput: {
                marginLeft: 36
            }
            // ... You can check the source to find the other keys.
            }}
            onDateChange={(date) => {this.setState({expectedDate: date})}}
      />

    )

    renderNotesFiled = () => {
        return (
            <View>
                <Field  style={{width:responsiveWidth(50) }} hiddenUnderLine={true} noFloating={true} textColor='black' name="notes" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.notes} component={InputComponent}
                    returnKeyType="done"
                    labelColor={this.state.notesborderColor?'#00397A':'#A6A6A6'}
                    onFocus={()=>{this.setState({notesborderColor:'#FF9900'})}}
                    onBlur={()=>{this.setState({notesborderColor:null})}}
                    onSubmit={() => {
                       Keyboard.dismiss()
                    }}
                />
            </View>
        )        
    }

    renderLoginButtons() {
       // const { handleSubmit } = this.props;
        return (
            <Button
            //onPress={handleSubmit(this.onLogin.bind(this))}
            style={{marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:'#06134C',borderRadius:moderateScale(2.5)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.add} color='white' />
            </Button>
        );
    }

    render(){
        return(
            <View>
              
                <AppHeader title={Strings.addTask} showBack/>
               <View style={{ marginTop:moderateScale(10), backgroundColor:'#F8F8F8', height:responsiveHeight(9), alignSelf:'center', borderWidth:moderateScale(0.5),width:responsiveWidth(85),borderColor:this.state.taskBordercolor?this.state.taskBordercolor:'#DADADA',borderRadius:moderateScale(2)}}>
                    {this.renderTaskFiled()}
               </View>
                
               <View style={{marginTop:moderateScale(10),alignItems:'center'}}>
                   <AppText text={Strings.date} color='#A6A6A6' />
               </View>

                {/* date */}
               <View style={{marginTop:moderateScale(10),alignItems:'center'}}>
                    {this.renderDate()}
               </View>



               <View style={{ marginTop:moderateScale(10), backgroundColor:'#F8F8F8', height:responsiveHeight(9), alignSelf:'center', borderWidth:moderateScale(0.5),width:responsiveWidth(85),borderColor:this.state.incomingNumberBorderColor?this.state.incomingNumberBorderColor:'#DADADA',borderRadius:moderateScale(2)}}>
                    {this.renderIncomingNumberFiled()}
               </View>
                
               <View style={{marginTop:moderateScale(10),alignItems:'center'}}>
                   <AppText text={Strings.expectedCompletionaDate} color='#A6A6A6' />
               </View>

                {/* date */}
               <View style={{marginTop:moderateScale(10),alignItems:'center'}}>
                    {this.renderExpectedDate()}
               </View>

               <View style={{ marginTop:moderateScale(10), backgroundColor:'#F8F8F8', height:responsiveHeight(9), alignSelf:'center', borderWidth:moderateScale(0.5),width:responsiveWidth(85),borderColor:this.state.notesborderColor?this.state.notesborderColor:'#DADADA',borderRadius:moderateScale(2)}}>
                    {this.renderNotesFiled()}
               </View>

               <View style={{alignSelf:'center'}}>
                   {this.renderLoginButtons()}
               </View>

            </View>
        );
    }
}

const form = reduxForm({
    form: "AkfaAddTask",
    validate,
})(AkfaAddTask)

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
})

export default connect(mapToStateProps)(form);