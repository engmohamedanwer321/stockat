import React, { Component } from 'react';
import {
  View,Image,TouchableOpacity,StatusBar,ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import {Icon,Button} from 'native-base';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import Strings from '../assets/strings';

import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);

class Welcome extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    componentDidMount(){    
        this.disableDrawer();     
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    

    render(){
        const {isRTL}=this.props
        console.log(isRTL)
        return(
            <ImageBackground style={{flex:1,justifyContent:'center',alignItems:'center'}} source={require('../assets/imgs/boodyCarBackground.png')} >
                <StatusBar backgroundColor={colors.primaryColor} />
                <View style={{height:responsiveHeight(70), marginTop:moderateScale(20), justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:responsiveWidth(80),height:responsiveHeight(10)}} source={require('../assets/imgs/bluelogo.png')} />
                    <View style={{justifyContent:'center',alignItems:'center',width:responsiveWidth(100),marginTop:moderateScale(5)}}>
                        <AppText text={Strings.logoText} color={colors.darkPrimaryColor} fontSize={responsiveFontSize(2.5)} />
                    </View>
                    <View style={{marginTop:moderateScale(40)}}>
                        <MyButton 
                        onPress={()=>{
                            this.props.navigator.push({
                                screen: 'Login',
                                animated: true,
                            })
                        }}
                        style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:colors.primaryColor,borderRadius:moderateScale(2.5)}}>
                            <AppText text={Strings.login} color='white' fontSize={responsiveFontSize(3)} />
                        </MyButton>

                        <MyButton
                         onPress={()=>{
                            this.props.navigator.push({
                                screen: this.props.clientType=='CLIENT'? 'CustomerSignup' : 'OwnerSignup',
                                animated: true,
                            })
                        }}
                         style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:'transparent',borderRadius:moderateScale(2.5),borderColor:colors.primaryColor,borderWidth:moderateScale(.6),marginTop:moderateScale(10)}}>
                            <AppText text={Strings.signup} color={colors.primaryColor} fontSize={responsiveFontSize(3)} />
                        </MyButton>
                        
                    </View>   
                    
                </View>

                <MyTouchableOpacity
                onPress={()=>{
                    this.props.navigator.resetTo({
                        screen: 'CustomerHome',
                        animated: true,
                      });
                }}
                 style={{alignSelf:this.props.isRTL?'flex-start':'flex-end',flexDirection:this.props.isRTL? 'row-reverse':'row',marginHorizontal:moderateScale(10),justifyContent:'center',alignItems:"center" }}>
                    <AppText color={colors.skipIconColor} text={Strings.skip} fontSize={responsiveFontSize(3.5)} />
                    <Icon name={this.props.isRTL?"chevron-left":"chevron-right"} type="Entypo" style={{color:colors.skipIconColor}} size={responsiveFontSize(2)} />
                </MyTouchableOpacity>

            </ImageBackground>
        );
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    clientType: state.signup.clientType,
})


export default connect(mapToStateProps)(Welcome);

