import React,{Component} from 'react';
import {View,RefreshControl,ActivityIndicator,Alert} from 'react-native';
import { moderateScale, responsiveWidth,  responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Button} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import AppSpinner from '../common/AppSpinner';
import SnackBar from 'react-native-snackbar-component';
import NotificationOrderDetailsCard from '../components/NotificationOrderDetailsCard';
import LoadingOverlay from '../components/LoadingOverlay';


class NotificationOrderDetails extends Component {

    

    state = {
        loading: false,
        errorText: null,
        data: new DataProvider(),
        total:0,
        
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }

    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 2,
          (type, dim) => {
            dim.width = responsiveWidth(50);
            dim.height = responsiveHeight(39);
          },
        );
      }

    componentDidMount(){
        this.enableDrawer()
        this.getOrders();      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    
    getOrders = () => {
        //this.props.currentUser.token;
        //this.props.currentUser.user.id;
        //this.props.subject;
        this.setState({loading:true});
        axios.get(`${BASE_END_POINT}/users/2/orders/${this.props.subject}`,
             { 
                 headers: { Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMCIsImlzcyI6ImJvb2RyQ2FyIiwiaWF0IjoxNTQwNjUxMjk1NTUzLCJleHAiOjE1NDA2NTE5MDAzNTN9.vf2XkQfewGwBOyU4d-Yo7K6nu2xoVmHUDh-Xl5_DCgc' }
             }
             )
             .then(response => {
                this.setState({
                    loading:false,
                    data:new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(response.data.productOrders),
                    total:response.data.total
                   });
            
            console.log(response.data);
            console.log(response.data.productOrders);
          })
          .catch((error) => {
              console.log(error)
            this.setState({loading:false})
            if(!error.response){
                //this.setState({errorText:Strings.noConnection})
            }
          });        
    }
    

    renderRow = (type, data, row) => {
        console.log('row data');
        console.log(data)
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <NotificationOrderDetailsCard 
        onPress={()=>{
           /* this.props.navigator.push({
                screen:'ProductDetails',
                animated:true,
                passProps: {productData:data}
            })*/
        }}
        data={data.product}
        quantity= {data.count}
        />
    </View> 
     );
    }

    renderOredersScreen = () =>(
        <View style={{flex:1}}>
            {this.state.loading?
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator color={colors.primaryColor} size='large' style={{alignSelf:'center'}}/>
            </View>
            :
            <View style={{flex:1}}>
                <RecyclerListView
                layoutProvider={this.renderLayoutProvider}
                dataProvider={this.state.data}
                rowRenderer={this.renderRow}
                />
                <View style={{elevation:2,shadowOffset:{height:1,width:1}, width:responsiveWidth(100), position:'absolute',bottom:0, justifyContent:'center',alignItems:'center',flexDirection:this.props.isRTL?'row-reverse':'row', height:responsiveHeight(8),backgroundColor:'#82C141'}}>
                    <AppText paddingHorizontal={moderateScale(2)} text={Strings.totalPrice} color='white' fontSize={responsiveFontSize(2)} />
                    <AppText paddingHorizontal={moderateScale(2)} text={this.state.total} color={colors.skipIconColor} fontSize={responsiveFontSize(3)} />
                    <AppText text={Strings.sar} color='white' fontSize={responsiveFontSize(3)} />
                </View>
            </View>

            }

        </View>
    )
  
    render(){
        const {navigator,source} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.notificationDetails} showBack navigator={navigator}/>
                {this.renderOredersScreen()}
               
                
                <SnackBar
                visible={this.state.errorText!=null?true:false} 
                textMessage={this.state.errorText}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View>
        );
    }
}



const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser, 
})

export default connect(mapStateToProps)(NotificationOrderDetails);
