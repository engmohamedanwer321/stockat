import React, { Component } from 'react';
import {
  View,Image,ImageBackground,AsyncStorage,StatusBar,Alert,AppState,
} from 'react-native';
import { connect } from 'react-redux';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import * as Progress from 'react-native-progress';
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import {getUser,userToken,login} from '../actions/AuthActions';
import {userLocation} from '../actions/OrderAction';
import  changeLanguage from '../actions/LanguageActions';
import SnackBar from 'react-native-snackbar-component';
import FCM, { FCMEvent } from "react-native-fcm";
import { BASE_END_POINT } from '../AppConfig';
import axios from 'axios';
import LottieView from 'lottie-react-native';



// 01201320304
//12345678   
class SplashScreen extends Component {
    pass;
    state={
        showSnack: false,
        latLocation:null,
        lngLocation:null,
        notiFlag:0,
        stopNotiClick:false,
       
    } 

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props){
        super(props); 
                      
    }

    

    checkToken = async (token) => {
        t = await AsyncStorage.getItem('@BoodCarToken')
        if(t){
            this.props.userToken(t);
            console.log('check token true')
        }else{
            AsyncStorage.setItem('@BoodCarToken',token)
            this.props.userToken(token)
            console.log('check token false')
        }

        console.log("My Token")
        console.log(t)
        console.log(token)
    }
    
    componentDidMount(){  
        this.disableDrawer();
        /*this.props.navigator.push({
            screen:'ProductDetails',
        })*/
        console.log('cdm')
        this.checkLanguage();
        this.checkLogin();

        FCM.getFCMToken().then(token => {
            console.log(`TOKEN (getFCMToken)  ${token}`);
            this.checkToken(token);
        });
        FCM.on(FCMEvent.Notification, async (notif) => {
            console.log('Recived');
            console.log(notif.fcm.icon);            
            if(!notif.local_notification){
              if(AppState.currentState === 'active' ) {
                this.ServerNotification(notif.fcm.title,notif.fcm.body,true);
              }else if(AppState.currentState === 'background') {
                this.ServerNotification(notif.fcm.title,notif.fcm.body,false);
              }
            }
        }); 
                
      
       FCM.on(FCMEvent.RefreshToken, (token) => {
            AsyncStorage.setItem('@BoodCarToken',token)
            this.props.userToken(token)
        });
       /* axios.put(`${BASE_END_POINT}updateToken`, JSON.stringify({
            oldToken: this.props.userTokens,
            newToken: token,
          }), {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(rsponse=>{
            console.log("done done")
            AsyncStorage.setItem('@BoodCarToken',token)
            this.props.userToken(token)
        }).catch(error=>{
            console.log("token Error")
            console.log(error)
            console.log(error.response)
        })

        */

        
     
    }

        
    ServerNotification = (title,body,val) => {
        FCM.presentLocalNotification({
        channel: 'default',
        id: new Date().valueOf().toString(), // (optional for instant notification)
        title: title, // as FCM payload
        body: body, // as FCM payload (required)
        sound: "bell.mp3", // "default" or filename
        priority: "high", // as FCM payload
        click_action: "com.myapp.MyCategory", // as FCM payload - this is used as category identifier on iOS.
       
        ticker: "My Notification Ticker", // Android only
        auto_cancel: true, // Android only (default true)
        icon: "ic_launcher", // as FCM payload, you can relace this with custom icon you put in mipmap
       
       
        color: "red", // Android only
        vibrate: 300, // Android only default: 300, no vibration if you pass 0
        wake_screen: true, // Android only, wake up screen when notification arrives
        group: "group", // Android only
       
        ongoing: true, // Android only
        my_custom_data: "my_custom_field_value", // extra data you want to throw
        lights: true, // Android only, LED blinking (default false)
        show_in_foreground: val, // notification when app is in foreground (local & remote)
        
        click_action: () => console.log("test"),
    });
    }

    
    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    checkLogin = async () => {      
        const userJSON = await AsyncStorage.getItem('@BoodyCarUser');
        if(userJSON){
            const userInfo = JSON.parse(userJSON);
            this.props.getUser(userInfo);
            console.log(this.props.currentUser)       
            this.props.navigator.resetTo({
                 screen: 'Home',         
                 animated: true,
             });         
        }else{
            this.props.navigator.resetTo({
                screen: 'AppIntro',
                animated: true,
            });
        }
        
    }

    checkLanguage = async () => {
        console.log("lang0   "+lang)
       const lang = await AsyncStorage.getItem('@lang');
       console.log("lang   "+lang)
       if(lang==='ar'){
            this.props.changeLanguage(true);
            Strings.setLanguage('ar')
        }else{
            this.props.changeLanguage(false);
            Strings.setLanguage('en')
        }      
    }

   
    render(){
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}} source={require('../assets/imgs/boodyCarBackground.png')} >
                <StatusBar hidden />
                <Image style={{width:responsiveWidth(80),height:responsiveHeight(10)}} source={require('../assets/imgs/bluelogo.png')} />
                <View style={{alignSelf:'center',height:responsiveHeight(8), position:'absolute',bottom:moderateScale(10),right:0,left:0}}>
                <LottieView
                    source={require('../assets/animations/six_spoke_spinner.json')}
                    autoPlay
                    loop
                />
                </View>
            </View>
        );
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser : state.auth.currentUser,
    userTokens: state.auth.userToken,
})

const mapDispatchToProps = {
    getUser,
    changeLanguage,
    userLocation,
    userToken,
    login
}

export default connect(mapToStateProps,mapDispatchToProps)(SplashScreen);

