import React,{Component} from 'react';
import {TextInput, View,Keyboard,NetInfo,Alert} from 'react-native';
import {  moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Button,Item, Picker, Label,Icon} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import AppSpinner from '../common/AppSpinner';
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import SnackBar from 'react-native-snackbar-component';
import { RNToasty } from 'react-native-toasty';
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);



const validate = values => {
    const errors = {};

    const partName = values.partName
    

    if (partName == null) {
        errors.partName = Strings.require;
    }
    
    return errors;
};
class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class ProductSearch extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    state = {      
        companyLoad:false,
        selectedCompany:-1,
        companies:[],
        modelLoad:false,
        selectedModel:-1,
        models:[],
        partCategoryLoad:false,
        selectedPartCategory:-1,
        categorys:[],
        yeraLoad:false,
        selectedYear:-1,
        years:[],
        notConnection:null,
        errorText:null,
        
        index: this.props.index,
        routes: [
        { key: 'advancedSearchTab', title:Strings.advancedSearch},
        { key: 'normalSearchTab', title:Strings.normalSearch },
        ],
    };

    constructor(props){
        super(props);
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getCompanies();    
            }else{
                this.setState({notConnection:Strings.noConnection})
            }
          });
    }

    componentDidMount(){    
        this.disableDrawer();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({notConnection:null})
                    this.getCompanies(); 
                }
            }
          );
        
    }

    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    getCompanies = () =>{
        this.setState({companyLoad:true});
        axios.get(`${BASE_END_POINT}companies`)
        .then(response=>{
            console.log(response.data)
            this.setState({companyLoad:false,companies:response.data.data});
            //response.data.data[0].id
            this.getModels(response.data.data[0].id)
        }).catch(error=>{
            console.log('company')
            console.log(error.response) 
            if(!error.response){
                this.setState({notConnection:Strings.noConnection});
            }
            this.setState({companyLoad:false});

        })
    }

    getModels= (companyID) =>{
        this.setState({modelLoad:true});
        axios.get(`${BASE_END_POINT}models/${companyID}/companies`)
        .then(response=>{
            console.log('models')
            console.log(response.data)
            const id = response.data[0].id;
            console.log('id is  '+id)
            this.setState({modelLoad:false,models:response.data});
            this.getYears(id);
            this.getPartCategory(id);
        }).catch(error=>{
            console.log('models error')
            console.log(error) 
            if(!error.response){
                this.setState({notConnection:Strings.noConnection});
            }
            this.setState({modelLoad:false});

        })
    }

    getYears= (modelID) =>{
        this.setState({yeraLoad:true});
        axios.get(`${BASE_END_POINT}year/${modelID}/models`)
        .then(response=>{
            console.log('years')
            console.log(response.data)
            this.setState({yeraLoad:false,years:response.data});
        }).catch(error=>{
            console.log('years')
            console.log(error.response) 
            if(!error.response){
                this.setState({notConnection:Strings.noConnection});
            }
            this.setState({yeraLoad:false});

        })
    }

    getPartCategory= (modelID) =>{
        this.setState({partCategoryLoad:true});
        axios.get(`${BASE_END_POINT}categories/${modelID}/models`)
        .then(response=>{
            console.log(response.data)
            this.setState({partCategoryLoad:false,categorys:response.data});
        }).catch(error=>{
            console.log('cat')
            console.log(error.response) 
            if(!error.response){
                this.setState({notConnection:Strings.noConnection});
            }
            this.setState({partCategoryLoad:false});

        })
    }

    _renderTabBar = props => <TabBar {...props} style={{backgroundColor:colors.primaryColor}} />

    onSearch(values){
        this.props.navigator.push({
            screen:'SearchResults',
            animated: true,
            passProps:{
                searchData:{search:values.partName},
                source: 'normal'
            }
        })
    }

    NormalSearchTab = () =>  (
           <View style={{flex:1}}>
                <View style={{justifyContent:'center',alignItems:'center',width:responsiveWidth(100),marginTop:moderateScale(5)}}>
                    <View style={{width:responsiveWidth(90)}}>
                    <Field borderColor='gray' style={{ width: responsiveHeight(90) }} textColor={colors.darkPrimaryColor} name="partName" isRTL={this.props.isRTL}  marginTop={moderateScale(7)} label={Strings.searchByPartNAme} component={InputComponent}
                        returnKeyType="done"
                        onSubmit={() => {
                            Keyboard.dismiss()
                        }}
                    />
                    </View>
                    <View style={{marginTop:moderateScale(7)}}>
                        <MyButton
                        onPress={this.props.handleSubmit(this.onSearch.bind(this))}
                         style={{width:responsiveWidth(90), justifyContent:'center',alignItems:'center',backgroundColor:colors.primaryColor,borderRadius:moderateScale(3)}}>
                            <AppText fontSize={responsiveFontSize(3)} text={Strings.search} color='white'/>
                        </MyButton>
                    </View>
                </View>
           </View>
       )

    renderCompanyPicker = () => {
        console.log(this.state.companies);
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
             <Label style={{fontSize:responsiveFontSize(2),color:'#CCCCCC',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.company}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.companyLoad?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your Company"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedCompany}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedCompany:value});
                        if(value!=-1){
                            Alert.alert(value.toString());
                            this.getModels(value);
                        }
                     }}
                     >
                     <Picker.Item color={colors.skipIconColor}  label={Strings.selectcompany} value={-1}/>
                     {this.state.companies.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.companyname} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    renderModerPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
             <Label style={{fontSize:responsiveFontSize(2),color:'#CCCCCC',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.model}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.modelLoad?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedModel}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedModel:value,});
                        if(value!=-1){
                            this.getYears(value);
                            this.getPartCategory(value);
                        }
                       
                     }}
                     >
                        <Picker.Item color={colors.skipIconColor}  label={Strings.selectmodel} value={-1}/>
                     {this.state.models.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.modelname} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    renderYearPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
             <Label style={{fontSize:responsiveFontSize(2),color:'#CCCCCC',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.year}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.yeraLoad?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedYear}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedYear:value,});
                     }}
                     >
                     <Picker.Item color={colors.skipIconColor} label={Strings.selectyear} value={-1}/>
                     {this.state.years.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.year.toString()} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    renderPartCategoryPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
             <Label style={{fontSize:responsiveFontSize(2),color:'#CCCCCC',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.partCategory}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.partCategoryLoad?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedPartCategory}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedPartCategory:value,});
                     }}
                     >
                     <Picker.Item color={colors.skipIconColor}  label={Strings.selectcategory} value={-1}/>
                     {this.state.categorys.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.categoryname} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    AdvancedSearchTab = () =>  (
        <View style={{flex:1}}>
             <View style={{justifyContent:'center',alignItems:'center',width:responsiveWidth(100),marginTop:moderateScale(5)}}>
                 {this.renderCompanyPicker()}
                 {this.renderModerPicker()}
                 {this.renderYearPicker()}
                 {this.renderPartCategoryPicker()}
                 <View style={{marginTop:moderateScale(12)}}>
                        <MyButton
                         onPress={()=>{
                             let params='';
                             if(this.state.selectedCompany!=-1){
                                params+=`companyid=${this.state.selectedCompany}`
                             }
                             if(this.state.selectedModel!=-1){
                                params+=`&modelid=${this.state.selectedModel}`
                            }
                            if(this.state.selectedPartCategory!=-1){
                                params+=`&categoryid=${this.state.selectedPartCategory}`
                            }
                            if(this.state.selectedYear!=-1){
                                params+=`&yearid=${this.state.selectedYear}` 
                            }
                            if(params.length>0){
                                this.props.navigator.push({
                                    screen:'SearchResults',
                                    animated: true,
                                    passProps:{
                                        params: params,
                                        source: 'advanced'
                                    }
                                })
                            }else{
                                RNToasty.Warn({title:Strings.selectOne})
                            }
                         }}
                         style={{width:responsiveWidth(90), justifyContent:'center',alignItems:'center',backgroundColor:colors.primaryColor,borderRadius:moderateScale(3)}}>
                            <AppText fontSize={responsiveFontSize(3)} text={Strings.search} color='white'/>
                        </MyButton>
                    </View>
             </View>

             <SnackBar
                visible={this.state.notConnection!=null?true:false} 
                textMessage={this.state.notConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
             
        </View>
    )
   

    render(){
        const {navigator} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.search} showBack navigator={navigator}/>
                <TabView
                    
                    navigationState={this.state}
                    renderTabBar={this._renderTabBar}
                    renderScene={renderScene = ({ route }) => {
                        switch (route.key) {
                          case 'advancedSearchTab':
                            return <this.AdvancedSearchTab/>
                        case 'normalSearchTab':
                            return <this.NormalSearchTab/>
                          default:
                            return null;
                        }
                      }}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{height:responsiveHeight(6), width: responsiveWidth(100)}}
                />
                
            </View>
        )
    }
}

const form = reduxForm({
    form: "ProductSearch",
    validate,
})(ProductSearch)


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps,mapDispatchToProps)(form);
