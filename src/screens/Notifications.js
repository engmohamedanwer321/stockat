import React,{Component} from 'react';
import {View,RefreshControl,NetInfo,StyleSheet} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import NotificationCard from '../components/NotificationCard';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
  } from 'recyclerlistview';
  import AppSpinner from '../common/AppSpinner';
import {getNotifications} from '../actions/NotificationAction'; 
import SnackBar from 'react-native-snackbar-component';
import ListFooter from '../components/ListFooter';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';



class Notifications extends Component {
    page = 1;
    state={
        errorText:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }

    constructor(props) {
        super(props);    
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.props.getNotifications(this.page,false,this.props.currentUser.token);

            }else{
                this.setState({errorText:Strings.noConnection})
            }
          });
          
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(11);
          },
        );
      }

      componentDidMount(){
        this.enableDrawer()
       
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.props.getNotifications(this.page,true,this.props.currentUser.token);
                }
            }
          );
        
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }

    renderRow = (type, data, row) => {
        var arabic = /[\u0600-\u06FF]/;
     return (
         <View style={{ marginTop: moderateScale(3), justifyContent: 'center', alignItems: 'center' }}>
             <NotificationCard
              navigator={this.props.navigator}
              token={this.props.currentUser.token}
              noti={data}
               />
         </View>
     );
    }

    
    renderFooter = () => {
        return (
          this.props.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
             <ListFooter/>
            </View>
            : null
        )
      }
    

    render(){
        const {navigator} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={navigator} showBack  title={Strings.notifications} />
                <RecyclerListView                 
                    layoutProvider={this.renderLayoutProvider}
                    dataProvider={this.props.notifications}
                    rowRenderer={this.renderRow}
                    renderFooter={this.renderFooter}
                    onEndReached={() => {
                        if (this.page <= this.props.pages) {
                            console.log('kak')
                            this.page+=1;
                            this.props.getNotifications(this.page, false,this.props.currentUser.token);
                        }
                        //this.props.getNotifications(this.page++, false,this.props.currentUser.token);
    
                    }}
                    refreshControl={
                    <RefreshControl colors={["#B7ED03"]}
                        refreshing={this.props.refresh}
                        onRefresh={() => {
                            this.page = 1
                            this.props.getNotifications(this.page, true,this.props.currentUser.token);
                        }
                        }
                    />
                    }
                    onEndReachedThreshold={.5}
                />
                
                {this.props.logoutLoading&&<LoadingDialogOverlay title={Strings.waitLogout}/>}
                <SnackBar
                    visible={this.props.errorText != null ? true : false||this.state.errorText != null ? true : false}
                    textMessage={Strings.noConnection}
                    messageColor='white'
                    backgroundColor={colors.primaryColor}
                    autoHidingTime={5000}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    footerSpiner: {
        alignItems: 'center',
        alignSelf: "flex-end",
        flex: 1,
        width: '100%',
        marginVertical: 10,
        backgroundColor: colors.primaryColor
      }
    
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    notifications: state.noti.notifications,
    loading: state.noti.loading,
    refresh: state.noti.refresh,
    pages: state.noti.pages,
    errorText: state.noti.errorText,
    currentUser : state.auth.currentUser,
    logoutLoading: state.menu.logoutLoading,
});

const mapDispatchToProps = {
    getNotifications,
}

export default connect(mapStateToProps,mapDispatchToProps)(Notifications);
