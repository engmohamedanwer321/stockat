import React,{Component} from 'react';
import {View,TouchableOpacity,NetInfo,RefreshControl,ActivityIndicator} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Button,Thumbnail} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import logout from "../actions/LogoutActions";
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import ProfileProductCard from '../components/ProfileProductCard';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import { BASE_END_POINT} from '../AppConfig';
import axios from 'axios';
import { RNToasty } from 'react-native-toasty';
import ListFooter from '../components/ListFooter';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';


const MyButton =  withPreventDoubleClick(Button);


class CustomerProfile extends Component {
    
    page=1;
    state = {
        showDialog:false,
        deleteProductLoading:false,
        selectedProduct:null,
        products: new DataProvider(),
        flag:0,
        refresh:false,
        loading:false,
        pages:null, 
        noConnection:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }
    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(98);
            dim.height = responsiveHeight(16);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
               // this.props.getCategories();
               this.getUserProducts(1,false);
            }else{
                this.setState({noConnection:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})
                    this.getUserProducts(1,true);
                }
            }
          );
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }

    getUserProducts(page, refresh) {
        this.setState({loading:true})
        let uri = `${BASE_END_POINT}products/?ownerId=${this.props.currentUser.user.id}&page=${page}&limit=10`
        if (refresh) {
            this.setState({loading: false, refresh: true})
        } else {
            this.setState({refresh:false,loading:true})
        }
        axios.get(uri)
            .then(response => {
                console.log("products under user")
                console.log(response.data)
                this.setState({
                    loading:false,
                    refresh:false,
                    pages:response.data.pageCount,
                    noConnection:null,
                    products: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.products.getAllData(), ...response.data.data]),
                })
                if(page==1){
                    if(response.data.data.length==0){
                        RNToasty.Info({title:strings.noProducts})
                    }
                }
            }).catch(error => {
                this.setState({loading:false,refresh:false})
                console.log(error.response);
                if (!error.response) {
                    this.setState({noConnection:strings.noConnection})
                }
            })
    
    }
    
   

    renderRow = (type, data, row) => {
        console.log('data')
        console.log(data)
        return (
            <ProfileProductCard 
            navigator={this.props.navigator}
            data={data}
            deleteFunProduct={()=>{ 
                this.setState({showDialog:true,selectedProduct:data.id})
            }}
            />
        );
       }

       renderProductList = () => (
        <RecyclerListView
            layoutProvider={this.renderLayoutProvider}
            dataProvider={this.state.products}
            rowRenderer={this.renderRow}
            renderFooter={this.renderFooter}
            onEndReached={() => {

                if (this.page <= this.props.pages) {
                    this.page++;
                    this.getUserProducts(this.page, false);
                }

            }}
            refreshControl={<RefreshControl colors={["#B7ED03"]}
                refreshing={this.state.refresh}
                onRefresh={() => {
                    this.page = 1
                    this.getUserProducts(this.page, true);
                }
                } />}
            onEndReachedThreshold={.5}

        />

    )

       renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

      deleteProduct = () => {
          this.setState({deleteProductLoading:true})
        axios.delete(`${BASE_END_POINT}products/${this.state.selectedProduct}`)
        .then(response => {
          console.log('delete product Success')
          console.log(response)
          this.setState({deleteProductLoading:false,showDialog:false})
          RNToasty.Success({title: Strings.productDeleted}) 
          this.getUserProducts(1, true);
        }).catch(error=>{
            this.setState({deleteProductLoading:false,showDialog:false})
            console.log('error') 
            console.log(error.response)
            if (!error.response) {
                RNToasty.Error({title: Strings.noConnection})
                
              }
        });
      }

      renderDeleteProductDialog = () => (
        <Dialog
        onTouchOutside={()=>{
           // this.setState({showDialog:false})
        }}
          width={responsiveWidth(80)}
          height={responsiveHeight(30)}
          visible={this.state.showDialog}
          dialogTitle={<DialogTitle title={Strings.deleteProduct} />}
        >
         
            <View
              style={{
                width: responsiveWidth(70),
                alignSelf: "center",
                marginTop: moderateScale(3)
              }}
            >
           
    
              <View
                style={{
                  marginTop: moderateScale(5),
                  alignSelf: this.props.isRTL ? "flex-end" : "flex-start"
                }}
              >
                <AppText
                  text={Strings.sureDeleteProduct}
                  color={colors.darkPrimaryColor}
                  fontWeight="400"
                  fontSize={responsiveFontSize(2.5)}
                />
                
              </View>
              
              <View style={{marginVertical:moderateScale(12), alignSelf:'center',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Button
                    onPress={()=>{this.deleteProduct()}}
                     style={{marginHorizontal:moderateScale(3), borderColor:'transparent', width:responsiveWidth(20),justifyContent:'center',alignItems:'center',height:responsiveHeight(6),borderRadius:moderateScale(10),backgroundColor:colors.buttonColor,borderWidth:1}}>
                        <AppText text={Strings.ok} color='white' />
                    </Button>
                    <Button
                    onPress={()=>{
                        this.setState({showDialog:false});
                    }}
                     style={{backgroundColor:'red', borderColor:'transparent', width:responsiveWidth(20),justifyContent:'center',alignItems:'center',height:responsiveHeight(6),borderRadius:moderateScale(10),borderWidth:1,marginHorizontal:moderateScale(3)}}>
                        <AppText text={Strings.cancel} color='white' />
                    </Button>
              </View>
              
            </View>
          
        </Dialog>
      );
    
    render(){
        const {navigator,currentUser,userToken,isRTL} = this.props;

        return(
            <View style={{flex:1}}>
                <AppHeader navigator={navigator}  showBurger title={Strings.profile} />
                <View style={{justifyContent:'center',alignItems:'center', width:responsiveWidth(100),backgroundColor:colors.darkPrimaryColor}}> 
                   
                    <View style={{ justifyContent:'space-between', width:responsiveWidth(97), flexDirection:isRTL?'row-reverse':'row', marginVertical:moderateScale(2)}}>
                        <AppText text='' />
                        <AppText fontSize={responsiveFontSize(3)} fontWeight='400' color='white' text={`${currentUser.user.firstname} ${currentUser.user.lastname}`}/>
                        <TouchableOpacity
                        onPress={()=>{
                            navigator.push({
                                screen: 'UpdateProfile',
                                animated: true,
                            })
                        }}
                        >
                             <Icon name='cog' type='FontAwesome'  style={{fontSize:22, color:'white'}} />
                        </TouchableOpacity>
                    </View>
                    <AppText color='white' text={currentUser.user.phone}/>
                    <AppText color='white' text={currentUser.user.email}/>
                    <Thumbnail
                    large                    
                    source={currentUser.user.img?{uri:currentUser.user.img}:require('../assets/imgs/food1.jpg')}
                    style={{borderWidth:2,borderColor:'white', marginTop:moderateScale(7)}}
                    />
            
                </View>
                <View style={{marginHorizontal:moderateScale(6),marginVertical:moderateScale(5), alignSelf:isRTL?'flex-end':'flex-start'}}>
                    <AppText text={Strings.myProducts} color={colors.buttonColor} fontSize={responsiveFontSize(2.6)} fontWeight='400'  />
                </View>
                {this.renderProductList()}
                {this.renderDeleteProductDialog()}
            </View>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser : state.auth.currentUser,
    userToken: state.auth.userToken,
    logoutLoading: state.menu.logoutLoading,
});
const mapDispatchToProps = {
    logout,
  }

export default connect(mapStateToProps,mapDispatchToProps)(CustomerProfile);
