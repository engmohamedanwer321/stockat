import React,{Component} from 'react';
import {View,RefreshControl,StyleSheet,NetInfo} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Button} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import {getCompanyProducts} from '../actions/ProductAction';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import AppSpinner from '../common/AppSpinner';
import SnackBar from 'react-native-snackbar-component';
import ProductCard from '../components/ProductCard';
import { RNToasty } from 'react-native-toasty';
import {AddProductToBacket} from '../actions/OrderAction';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';




class CompanyProducts extends Component {

    page=1;
    state= {
        errorText:null,
        companyProducts: new DataProvider(),
        refresh:false,
        loading:false,
        loading:false,
        pages:null,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }

    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 2,
          (type, dim) => {
            dim.width = responsiveWidth(50);
            dim.height = responsiveHeight(33);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getCompanyProducts(this.props.companyID,this.page,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
      }

    componentDidMount(){
        this.enableDrawer()     
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getCompanyProducts(this.props.companyID,this.page,true);
                }
            }
          );      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    getCompanyProducts(companyID, page, refresh) {
            this.setState({loading:true})
            let uri = `${BASE_END_POINT}products/companies/${companyID}/products?page=${page}&limit=10`
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false,loading:true})
            }
            axios.get(uri)
                .then(response => {
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        companyProducts: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.companyProducts.getAllData(), ...response.data.data]),
                    })
                    if(response.data.data.length==0){
                        RNToasty.Info({title:Strings.notResults})
                    }
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                    console.log(error.response);
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <ProductCard 
       data={data}
        onPress={()=>{
            this.props.navigator.push({
                screen:'ProductDetails',
                animated:true,
                passProps: {productData:data}
            })
        }}
        buyProduct={()=>{
            this.props.AddProductToBacket({
                product:data.id,
                count:1
            },data)
            RNToasty.Info({title:Strings.addProductSuccessfuly})
        }}
        />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,currentUser,companyName} = this.props;
        console.log(companyName)
        return(
            <View style={{flex:1}}>
                <AppHeader title={`${companyName} Parts`} showBack navigator={navigator}/>
                {this.state.errorText==null&&
                <RecyclerListView            
                layoutProvider={this.renderLayoutProvider}
                dataProvider={this.state.companyProducts}
                rowRenderer={this.renderRow}
                renderFooter={this.renderFooter}
                onEndReached={() => {
                   
                    if(this.page <= this.props.pages){
                        this.page++;
                        this.getCompanyProducts(this.props.companyID,this.page, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.refresh}
                    onRefresh={() => {
                      this.page = 1
                      this.getCompanyProducts(this.props.companyID,this.page, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}
        
            />
                }
                 <SnackBar
                visible={this.state.errorText!=null?true:false} 
                textMessage={Strings.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    footerSpiner: {
        alignItems: 'center',
        alignSelf: "flex-end",
        flex: 1,
        width: '100%',
        marginVertical: 10,
        backgroundColor: colors.primaryColor
      }
    
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    products: state.product.products,
    loading: state.product.loading,
    refresh: state.product.refresh,
    pages: state.product.pages,
    errorText: state.product.errorText,
})

const mapDispatchToProps = {
    getCompanyProducts,
    AddProductToBacket,
}

export default connect(mapStateToProps,mapDispatchToProps)(CompanyProducts);
