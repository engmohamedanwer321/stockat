import React,{Component} from 'react';
import {FlatList, View,TouchableOpacity,Alert,NetInfo,AppState} from 'react-native';
import {  moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Button,Thumbnail} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import MapView, { Marker } from 'react-native-maps';
import ActionButton from 'react-native-action-button';
import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import logout from "../actions/LogoutActions";
import SnackBar from 'react-native-snackbar-component';
import {getCategories} from '../actions/CategoryAction';
import {getOwnerCompanies} from '../actions/CompanyAction';
import ListFooter from '../components/ListFooter';
import OptionsMenu from "react-native-options-menu";
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';



class OwnerHome extends Component {
 

 
    state = {
        companyImages:this.props.currentUser.user.company.filter(company=>{
            return {img:company.img}
        }),
        errorText:null,
        index: 0,
        routes: [
        { key: 'myProfileTab', title:Strings.profile},
        { key: 'myProductsTab', title:Strings.myProducts },
        ],
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props){
        super(props);          
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.props.getCategories();
                this.props.getOwnerCompanies();
            }else{
                this.setState({errorText:Strings.noConnection})
            }
          });
    }
 

    componentDidMount(){      
        this.disableDrawer();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.props.getCategories();
                    this.props.getOwnerCompanies();                }
            }
          );      
    }

   

    /*getCategories = () => {
        this.setState({loading:true});
        axios.get(`${BASE_END_POINT}/categories`)
        .then(response=>{
            this.setState({loading:false,categories:response.data.data});
        })
        .catch(error=>{
         this.setState({loading:false,noConnection:Strings.noConnection});
        })
    }*/

    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    leftComponent = () => (     
            <View style={{flexDirection:this.props.isRTL? 'row-reverse' : 'row'}}>
            
            <TouchableOpacity
                    onPress={()=>{
                        this.props.navigator.push({
                            screen:'Notifications',
                            animated:true,
                        })
                    }}
                    style={{marginHorizontal:moderateScale(7)}}>
                        <Icon name='bell' color='white' size={20} />
            </TouchableOpacity> 

                <TouchableOpacity
                onPress={() => {
                    if (this.props.currentUser) {
                        this.props.navigator.push({
                            screen: 'OwnerUpdateProfile',
                            animated: true,
                        })
                    } else {
                        RNToasty.Warn({ title: strings.checkUser })
                    }
                }}
                style={{ marginHorizontal: moderateScale(7) }}>
                <Icon name='edit' color='white'  size={20} />
            </TouchableOpacity>

            <OptionsMenu
                button={require('../assets/imgs/menu.png')}
                buttonStyle={{ width: 10, height: 15, margin: 3, resizeMode: "contain" }}
                destructiveIndex={1}
                options={[Strings.languauge, Strings.logOut]}
                actions={
                    [()=>{
                        this.props.navigator.push({
                            screen:'SelectLanguage',
                            animated:true,
                        })
                    },
                    ()=>{
                        Alert.alert(
                            `${Strings.logoutTitle}`,
                            `${Strings.logoutMessage}`,
                            [
                              {text: `${Strings.no}`},
                              {text: `${Strings.yes}`, onPress: () =>{
                                this.props.logout(this.props.userToken,this.props.currentUser.token ,this.props.navigator)
                              } },
                            ],
                          )
                    }
                ]}
                />
             
            </View>              
   )

   _renderTabBar = props => 
   <TabBar
    {...props} 
     style={{backgroundColor:colors.primaryColor}} />

    MyProfileTab = () => {
       const {currentUser,isRTL,orders} = this.props; 
        return (
            currentUser&&
                <View style={{flex:1}}>
                <View style={{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(100), height: responsiveHeight(40), backgroundColor: '#F7F7F7' }}>
                    <Icon name='user' type='FontAwesome' style={{ fontSize: responsiveFontSize(8), color: '#979797' }} />
                    <View style={{width:responsiveWidth(93),justifyContent:'center',alignItems:'center'}}>
                        <AppText textAlign='center' color={colors.primaryColor} fontSize={responsiveFontSize(5)} text={`${currentUser.user.firstname} ${currentUser.user.lastname}`} />
                    </View>
                    <AppText text={currentUser.user.email} color='gray' />
                    <AppText text={currentUser.user.phone} color={colors.primaryColor} />
                    <View style={{marginTop:moderateScale(3)}}>
                        <Button
                         onPress={()=>{
                            this.props.navigator.push({
                                 screen: 'ChangePassword',         
                                 animated: true,
                             });
                         }}
                         style={{width:responsiveWidth(60),borderRadius:moderateScale(4), justifyContent:'center',alignItems:'center',backgroundColor:colors.primaryColor}}>
                            <AppText color='white' text={Strings.changPassword} />
                        </Button>
                    </View>
                </View>
                <View style={{height:responsiveHeight(8), flexDirection:isRTL? 'row-reverse':'row',justifyContent:'space-around',alignItems:'center'}}>
                    <AppText paddingHorizontal={moderateScale(10)} fontSize={responsiveFontSize(2.5)} fontWeight='600'  color={colors.primaryColor} text={Strings.company} />                   
                        <FlatList
                        inverted={!isRTL && -1}
                        style={{marginHorizontal:moderateScale(10)}}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={this.state.companyImages}
                        renderItem={({item,index})=>(
                            <Thumbnail style={{marginHorizontal:moderateScale(2.2)}}  small source={{uri:item.img}} />
                        )}                    
                        />
                    
                </View>
                <View style={{flex:1}}>
                <MapView
                        showsMyLocationButton={true}
                        zoomControlEnabled={true}
                        style={{ flex:1}}
                        initialRegion={{
                            latitude: 1.6456, //currentUser.user.location[0],
                            longitude: 2.21456,// currentUser.user.location[1],
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    >
                        <Marker pinColor='red' coordinate={{
                             latitude: 1.6456,  //currentUser.user.location[0],
                             longitude:2.21456, // currentUser.user.location[1],
                        }} />
                    </MapView>
                </View>
                <SnackBar
                visible={this.props.ownerCompanyErrorText!=null?true:false||this.state.errorText!=null?true:false} 
                textMessage={Strings.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />         
            </View>

        );
    }

    listItem = (item) => (
        <TouchableOpacity
        onPress={()=>{
            this.props.navigator.push({
                screen: 'OwnerProducts',
                animated: true,
                passProps:{categoryID:item.id}
            })
        }}
         style={{borderBottomWidth:0.4,borderBottomColor:'#DEDEDE', height:responsiveHeight(11), flexDirection:this.props.isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between'}}>
            <AppText paddingHorizontal={moderateScale(10)} color={colors.primaryColor} fontSize={responsiveFontSize(3)} text={item.categoryname}/>
            <TouchableOpacity
               /* onPress={()=>{
                    this.props.navigator.resetTo({
                        screen: 'CustomerHome',
                        animated: true,
                      });
                }}*/
                 style={{marginHorizontal:moderateScale(10),justifyContent:'center',alignItems:"center" }}>
                    
                    <Icon name={this.props.isRTL?"chevron-left":"chevron-right"} type="Entypo" style={{color:colors.skipIconColor}} size={responsiveFontSize(2)} />
                </TouchableOpacity>
        </TouchableOpacity>
    )

    MyProductsTab = () => {
        const {isRTL}=this.props;
        return(
            <View style={{flex:1}}>

                {
                    this.props.categoryLoad?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ListFooter/>
                        </View>
                    :
                        <View style={{ flex: 1 }} >
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                data={this.props.categories}
                                renderItem={({ item }) => this.listItem(item)}
                            />

                           
                        </View>
                }
                <SnackBar
                visible={this.props.errorText!=null?true:false||this.state.errorText!=null?true:false} 
                textMessage={Strings.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />

            </View>
        )
    }

   
    render(){
        return(
            <View style={{flex:1}}>
               <AppHeader leftComponent={this.leftComponent()} title={this.state.index==0?Strings.profile:Strings.myProducts}  navigator={this.props.navigator}/>
               <TabView      
                    
                    navigationState={this.state}
                    renderTabBar={this._renderTabBar}
                    renderScene={renderScene = ({ route }) => {
                        switch (route.key) {
                          case 'myProfileTab':
                            return <this.MyProfileTab />
                          case 'myProductsTab':
                            return <this.MyProductsTab/>
                          default:
                            return null;
                        }
                      }}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{height:responsiveHeight(6), width: responsiveWidth(100)}}
                />
                {this.props.logoutLoading&&<LoadingDialogOverlay title={Strings.waitLogout}/>}
            </View>
        )
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    userToken: state.auth.userToken,
    orders: state.order.orders,
    categoryLoad: state.category.categoryLoad,
    categories: state.category.categories,
    errorText: state.category.errorText,
    ownerCompanies: state.company.ownerCompanies,
    ownerCompanyLoading: state.company.ownerCompanyLoading,
    ownerCompanyErrorText: state.company.ownerCompanyErrorText,
    logoutLoading: state.menu.logoutLoading,
})

const mapDispatchToProps = {
    getCategories,
    getOwnerCompanies,
    logout,
    
}

export default connect(mapToStateProps,mapDispatchToProps)(OwnerHome);
