import { Navigation } from 'react-native-navigation';
import { Provider } from "react-redux";
import store from "../store";
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

import MenuContent from "../components/MenuContent";
import SplashScreen from '../screens/SplashScreen';
import AppIntro from './AppIntro';
import About from './About';
import ContactUs from './ContactUs';
import Favourites from './Favourites';





import UserType from  '../screens/UserType';
import Welcome from  '../screens/Welcome';
import Login from '../screens/Login';
import Signup from '../screens/Signup';
import Home from '../screens/Home';
import CustomerAdvertisement from './CustomerAdvertisement';
import OwnerHome from '../screens/OwnerHome';
import TermsAndCondictions from '../screens/TermsAndCondictions';
import OwnerSignup from '../screens/OwnerSignup';
import CustomerCompanies from './CustomerCompanies';
import SelectLanguage from './SelectLanguage';
import Notifications from './Notifications';
import NotificationOrderDetails from './NotificationOrderDetails'
import CustomerProfile from './CustomerProfile'
import CompanyProducts from './CompanyProducts';
import ProductSearch from './ProductSearch';
import SearchResults from './SearchResults';
import ProductDetails from './ProductDetails';
import CustomerShoppingBacket from './CustomerShoppingBacket';
import OwnerProducts from './OwnerProducts';
import OwnerProductDetails from './OwnerProductDetails';
import AddNewProduct from './AddNewProduct';
import Success from './Success';
import Wait from './Wait';
import NotificationProductDetails from './NotificationProductDetails';
import Accept from './Accept';
import AkfaAddTask from './AkfaAddTask';
import WaitSearchResponse from './WaitSearchResponse';
import ChangePassword from './ChangePassword';
import UpdateProfile from './UpdateProfile';
import NotificationSearchedProductResponse from './NotificationSearchedProductResponse';
import OwnerUpdateProfile from './OwnerUpdateProfile';
import ForgetPassword from './ForgetPassword';
import ProductDetailsForHome from './ProductDetailsForHome'
import UpdateProduct from './UpdateProduct';

export function registerScreens() {
     Navigation.registerComponent("SplashScreen", () => SplashScreen, store, Provider);
     Navigation.registerComponent("AppIntro", () =>gestureHandlerRootHOC(AppIntro), store, Provider);
     Navigation.registerComponent("About", () =>gestureHandlerRootHOC(About), store, Provider);
     Navigation.registerComponent("ContactUs", () =>gestureHandlerRootHOC(ContactUs), store, Provider);
     Navigation.registerComponent("Favourites", () =>gestureHandlerRootHOC(Favourites), store, Provider);
     Navigation.registerComponent("ProductDetailsForHome", () =>gestureHandlerRootHOC(ProductDetailsForHome), store, Provider);
     Navigation.registerComponent("UpdateProduct", () =>gestureHandlerRootHOC(UpdateProduct), store, Provider);





     
     Navigation.registerComponent("OwnerSignup", () => gestureHandlerRootHOC(OwnerSignup), store, Provider);
     Navigation.registerComponent("UserType", () =>  gestureHandlerRootHOC(UserType), store, Provider);
     Navigation.registerComponent("Welcome", () =>  gestureHandlerRootHOC(Welcome), store, Provider);
     Navigation.registerComponent("Login", () => gestureHandlerRootHOC(Login), store, Provider);
     Navigation.registerComponent("MenuContent", () => gestureHandlerRootHOC(MenuContent), store, Provider);
     Navigation.registerComponent("Signup", () => gestureHandlerRootHOC(Signup), store, Provider);
     Navigation.registerComponent("Home", () => gestureHandlerRootHOC(Home), store, Provider)
     Navigation.registerComponent("OwnerHome", () => gestureHandlerRootHOC(OwnerHome), store, Provider);
     Navigation.registerComponent("TermsAndCondictions", () => gestureHandlerRootHOC(TermsAndCondictions), store, Provider);  
     Navigation.registerComponent("CustomerCompanies", () => gestureHandlerRootHOC(CustomerCompanies), store, Provider);
     Navigation.registerComponent("SelectLanguage", () => gestureHandlerRootHOC(SelectLanguage), store, Provider);
     Navigation.registerComponent("Notifications", () => gestureHandlerRootHOC(Notifications), store, Provider);
     Navigation.registerComponent("Notifications", () => gestureHandlerRootHOC(Notifications), store, Provider);
     Navigation.registerComponent("NotificationOrderDetails", () => gestureHandlerRootHOC(NotificationOrderDetails), store, Provider);
     Navigation.registerComponent("CustomerProfile", () => gestureHandlerRootHOC(CustomerProfile), store, Provider);
     Navigation.registerComponent("CompanyProducts", () => gestureHandlerRootHOC(CompanyProducts), store, Provider);
     Navigation.registerComponent("ProductSearch", () => gestureHandlerRootHOC(ProductSearch), store, Provider);
     Navigation.registerComponent("SearchResults", () => gestureHandlerRootHOC(SearchResults), store, Provider);
     Navigation.registerComponent("ProductDetails", () => gestureHandlerRootHOC(ProductDetails), store, Provider);
     Navigation.registerComponent("CustomerShoppingBacket", () => gestureHandlerRootHOC(CustomerShoppingBacket), store, Provider);
     Navigation.registerComponent("OwnerProducts", () => gestureHandlerRootHOC(OwnerProducts), store, Provider);
     Navigation.registerComponent("OwnerProductDetails", () => gestureHandlerRootHOC(OwnerProductDetails), store, Provider);
     Navigation.registerComponent("AddNewProduct", () => gestureHandlerRootHOC(AddNewProduct), store, Provider);
     Navigation.registerComponent("Success", () => gestureHandlerRootHOC(Success), store, Provider);
     Navigation.registerComponent("Wait", () => gestureHandlerRootHOC(Wait), store, Provider);
     Navigation.registerComponent("Accept", () => gestureHandlerRootHOC(Accept), store, Provider);
     Navigation.registerComponent("NotificationProductDetails", () => gestureHandlerRootHOC(NotificationProductDetails), store, Provider);
     Navigation.registerComponent("CustomerAdvertisement", () => gestureHandlerRootHOC(CustomerAdvertisement), store, Provider);
     Navigation.registerComponent("WaitSearchResponse", () => gestureHandlerRootHOC(WaitSearchResponse), store, Provider);
     Navigation.registerComponent("NotificationSearchedProductResponse", () => gestureHandlerRootHOC(NotificationSearchedProductResponse), store, Provider);
     Navigation.registerComponent("ChangePassword", () => gestureHandlerRootHOC(ChangePassword), store, Provider);
     Navigation.registerComponent("UpdateProfile", () => gestureHandlerRootHOC(UpdateProfile), store, Provider);
     Navigation.registerComponent("OwnerUpdateProfile", () => gestureHandlerRootHOC(OwnerUpdateProfile), store, Provider);
     Navigation.registerComponent("ForgetPassword", () => gestureHandlerRootHOC(ForgetPassword), store, Provider);

     //Akfa
     Navigation.registerComponent("AkfaAddTask", () => AkfaAddTask, store, Provider);

    }

