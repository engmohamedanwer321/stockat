import React, { Component } from 'react';
import {
  View,Image,ImageBackground,TouchableOpacity,StatusBar
} from 'react-native';
import { connect } from 'react-redux';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import Icon from 'react-native-vector-icons/FontAwesome5';
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import Swiper from 'react-native-swiper';




// 01201320304
//12345678   
class AppIntro extends Component {
   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    componentDidMount(){  
        this.disableDrawer();
    }
    
    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    
    renderAppIntroFirstPage = () =>(
        <ImageBackground style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} source={require('../assets/imgs/boodyCarBackground.png')} >        
            <Image style={{ width: responsiveWidth(80), height: responsiveHeight(10) }} source={require('../assets/imgs/bluelogo.png')} />
            <View style={{ justifyContent:'center',alignItems:'center' ,position: 'absolute', bottom: moderateScale(20), right: 0, left: 0 }}>
                <AppText color='black' fontSize={responsiveFontSize(3)} text={Strings.easyToUse} />
                <View style={{width:responsiveWidth(80), alignSelf:'center',marginVertical:moderateScale(10)}}>
                    <AppText textAlign='center' color='gray' fontSize={responsiveFontSize(2)} text={Strings.appIntroTitle1} />
                </View>
            </View>
        </ImageBackground>
    )

    renderAppIntroSecondPage = () =>(
        <ImageBackground style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} source={require('../assets/imgs/boodyCarBackground.png')} >        
            <Image style={{ width: responsiveWidth(80), height: responsiveHeight(10) }} source={require('../assets/imgs/bluelogo.png')} />
            <View style={{justifyContent:'center',alignItems:'center' ,position: 'absolute', bottom: moderateScale(20), right: 0, left: 0 }}>
                <AppText color='black' fontSize={responsiveFontSize(3)} text={Strings.buyAndsell} />
                <View style={{width:responsiveWidth(80), alignSelf:'center',marginVertical:moderateScale(10)}}>
                    <AppText textAlign='center' color='gray' fontSize={responsiveFontSize(2)} text={Strings.appIntroTitle2} />
                </View>
            <View style={{alignSelf:this.props.isRTL?'flex-start':'flex-end', marginHorizontal:moderateScale(10)}}>
                <TouchableOpacity
                onPress={()=>{
                    this.props.navigator.resetTo({
                        screen: 'Login',
                        animated: true,
                    });
                }}
                >
                    <Icon name={this.props.isRTL?'chevron-left':'chevron-right'} color='black' size={20} />
                </TouchableOpacity>
            </View>
            </View>

            
        </ImageBackground>
    )
   
    render(){
        return(
           <View style={{flex:1}}>
            <StatusBar hidden />
                <Swiper
                autoplay={false}
                loop={false}
                activeDotColor='black'
                dotColor='gray'
                >
                    {this.renderAppIntroFirstPage()}
                    {this.renderAppIntroSecondPage()}
                </Swiper>
           </View> 
        );
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
})


export default connect(mapToStateProps)(AppIntro);

