import React,{Component} from 'react';
import {View,Image,AsyncStorage} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import {Button} from 'native-base';
import Strings from '../assets/strings';
import AppText from '../common/AppText'
import AppHeader from '../common/AppHeader';
import  changeLanguage from '../actions/LanguageActions';



class SelectLanguage extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }
     
    render(){
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={this.props.navigator} showBurger  title={Strings.selectLanguage} />
                <Image style={{alignSelf:'center',marginVertical:moderateScale(30), width:responsiveWidth(80),height:responsiveHeight(10)}} source={require('../assets/imgs/bluelogo.png')} />
                <View style={{alignSelf:'center'}}>
                    <Button
                    onPress={()=>{
                        this.props.changeLanguage(false);
                        Strings.setLanguage('en');
                        AsyncStorage.setItem('@lang','en')
                        console.log('lang   '+ this.props.isRTL)
                    }}
                     style={{backgroundColor:colors.darkPrimaryColor,justifyContent:'center',alignItems:'center',width:responsiveWidth(50),height:responsiveHeight(8),borderRadius:moderateScale(10)}} >
                        <AppText fontSize={responsiveFontSize(2.8)} text='English' color='white'/>
                    </Button>

                    <Button
                     onPress={()=>{
                        this.props.changeLanguage(true);
                        Strings.setLanguage('ar');
                        AsyncStorage.setItem('@lang','ar')
                        console.log('lang   '+ this.props.isRTL)
                    }}
                     style={{marginTop:moderateScale(10),backgroundColor:colors.buttonColor,justifyContent:'center',alignItems:'center',width:responsiveWidth(50),height:responsiveHeight(8),borderRadius:moderateScale(10)}} >
                        <AppText fontSize={responsiveFontSize(2.8)} text='العربية' color='white'/>
                    </Button>
                </View>
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
   
})

const mapDispatchToProps = {
    changeLanguage,
}


export default connect(mapToStateProps,mapDispatchToProps)(SelectLanguage);