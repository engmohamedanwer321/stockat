import React, { Component } from 'react';
import {
  View,StatusBar,TouchableOpacity,Keyboard,ScrollView,Image
} from 'react-native';
import { connect } from 'react-redux';
import { Button,  Icon, Item, Picker, Label } from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import AppSpinner from '../common/AppSpinner';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import Checkbox from 'react-native-custom-checkbox';
import { signup } from '../actions/SignupAction';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import SnackBar from 'react-native-snackbar-component';
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);



const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }

    if (!values.firstName) {
        errors.firstName = Strings.require;
    } else if (!isNaN(values.firstName)) {
        errors.firstName = Strings.errorName;
    }

    if (!values.lastName) {
        errors.lastName = Strings.require;
    } else if (!isNaN(values.lastName)) {
        errors.lastName = Strings.errorName;
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = Strings.require;
    } else if (!values.phoneNumber.startsWith('05')) {
        errors.phoneNumber = Strings.errorStartPhone;
    }  else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = Strings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = Strings.errorPhoneFormat
    }

    if (!values.password) {
        errors.password = Strings.require;
    }

    if (!values.confirmPassword) {
        errors.confirmPassword = Strings.require;
    } else if (values.password != values.confirmPassword) {
        errors.confirmPassword = Strings.errorConfirmPassword;
    }
    
    return errors;
};

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class Signup extends Component {

    state = {
        agreeTerms:false,
        citiesLoadig:true,
        cities:[],
        selectedCity:null,
        areaLoading:true,
        areas:[],
        selectedArea:null,
        errorText:null,
        showSnack:false,
    }

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator; 
    }
    
    componentDidMount(){    
        this.disableDrawer();
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    

    onSignup(values) {
        if(this.state.agreeTerms){
            const user = {
                firstname:values.firstName,
                lastname:values.lastName,
                email:values.email,
                phone:values.phoneNumber,
                password:values.password,
                type:'CLIENT',
                token:this.props.userToken
            } 

            this.props.signup(user,this.props.navigator)                 
        }    
    }
    
    renderSignupButtons() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={
                handleSubmit(this.onSignup.bind(this)) 
            }
            style={{alignSelf:'center', opacity:this.state.agreeTerms? 1:0.6, marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(50),backgroundColor:colors.darkPrimaryColor,borderRadius:moderateScale(10)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.signup} color='white' />
            </MyButton>
        );
    }

    
    renderTerms = () => {
        return(
            <View style={{justifyContent:'center',alignItems:'center', marginTop:moderateScale(5), width:responsiveWidth(80), flexDirection:this.props.isRTL?'row-reverse':'row',marginHorizontal:moderateScale(2)}}>
                <View style={{marginRight:3, alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
                <Checkbox
                    checked={this.state.agreeTerms}
                    style={{backgroundColor: 'white', color:colors.primaryColor, borderRadius: 5}}
                    onChange={(name, checked) =>{this.setState({agreeTerms:checked})}}
                />
                </View>
                <View style={{ alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
                    <AppText text={Strings.term1} color='gray' fontSize={responsiveFontSize(2.5)}/>
                </View>
                <MyTouchableOpacity
                onPress={()=>{
                    this.props.navigator.push({
                        screen: 'TermsAndCondictions',
                        animated: true,
                    })
                }}
                 style={{ borderBottomWidth:1,borderBottomColor:colors.buttonColor, alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
                    <AppText text={Strings.term2} fontSize={responsiveFontSize(2.5)} color={colors.buttonColor}/>
                </MyTouchableOpacity>
        
            </View>
        )
    }
    
    renderContent() {
        const { isRTL } = this.props;
        return (
            <View>

                <View style={{width:responsiveWidth(80), flexDirection:isRTL?'row-reverse':'row'}}>
                    <View style={{width:responsiveWidth(39)}}>
                        <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} name="firstName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.firstName} component={InputComponent}
                        returnKeyType="done"
                            onSubmit={() => {
                                Keyboard.dismiss();
                            }}
                        />
                    </View>

                    <View style={{marginHorizontal:moderateScale(2), width:responsiveWidth(39)}}>
                        <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} name="lastName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.secondName} component={InputComponent} 
                            returnKeyType="done"
                            onSubmit={() => {
                                Keyboard.dismiss();
                            }}
                        />
                    </View>
                </View>

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} numeric name="phoneNumber" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.mobileNumber} component={InputComponent} 
                    returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />


                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} email name="email" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.email} component={InputComponent} 
                   returnKeyType="done"
                   onSubmit={() => {
                       Keyboard.dismiss();
                   }}
                />

                
                
                  
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} password  name="password" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.password} component={InputComponent} 
                    returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} password  name="confirmPassword" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.confirmPassword} component={InputComponent}
                   returnKeyType="done"
                   onSubmit={() => {
                       Keyboard.dismiss();
                   }}
                />
               
                {this.renderTerms()}
                {this.renderSignupButtons()}
            </View>

        )
    }

    render(){
        return(
            <ScrollView style={{ flex:1 }}>
            <Image style={{alignSelf:'center',marginTop:moderateScale(30), width:responsiveWidth(80),height:responsiveHeight(10)}} source={require('../assets/imgs/bluelogo.png')} />
               <View style={{marginTop:moderateScale(15),width:responsiveWidth(80),alignSelf:'center'}}>
                        {this.renderContent()}
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: this.props.isRTL ? 'row-reverse' : 'row', alignSelf: 'center', marginVertical:moderateScale(10)}}>
                    <AppText fontSize={responsiveFontSize(2.5)} text={Strings.alreadyHaveAccount} color='gray' />
                    <MyTouchableOpacity
                    onPress={()=>{
                        this.props.navigator.resetTo({
                            screen: 'Login',
                            animated: true,
                        });
                    }}
                    >
                        <AppText fontSize={responsiveFontSize(2.7)} text={`${Strings.login} Now`} color={colors.buttonColor} />
                    </MyTouchableOpacity>
                </View>    
                
                {this.props.signupLoading && <LoadingDialogOverlay title={Strings.checkSignup}/>}

               
                 {/* for signup */}
                <SnackBar
                visible={this.props.errorText!=null} 
                textMessage={this.props.errorText}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />

            </ScrollView>
            
        );
    }
}

const form = reduxForm({
    form: "Signup",
    validate,
})(Signup)

const mapDispatchToProps = {
    signup,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    signupLoading: state.signup.signupLoading,
    clientType: state.signup.clientType,
    errorText: state.signup.errorText,
    userToken: state.auth.userToken,
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

