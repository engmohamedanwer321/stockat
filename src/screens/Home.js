import React,{Component} from 'react';
import {
     View,TouchableOpacity, Alert,NetInfo,Keyboard,ActivityIndicator,
     Platform,TextInput,FlatList,RefreshControl
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Icon from 'react-native-vector-icons/FontAwesome5';
import strings from '../assets/strings';
import { Button } from "native-base";
import SnackBar from 'react-native-snackbar-component';
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import ActionButton from 'react-native-action-button';
import axios from 'axios';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import {getCategories} from '../actions/CategoryAction';
import FastImage from 'react-native-fast-image'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import ListFooter from '../components/ListFooter';

import HomeProductCard from '../components/HomeProductCard';

const MyButton =  withPreventDoubleClick(Button);
const MyTouchable =  withPreventDoubleClick(TouchableOpacity);



class Home extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
    list=[];
    page=1;
    state = {  
        categoryLoading:true,      
        noConnection:null,
        products: new DataProvider(),
        searchProducts: new DataProvider(),
        flag:0,
        refresh:false,
        loading:false,
        pages:null, 
        categories:[],
        selectedCategory: null,
    };

    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(80);
            dim.height = responsiveHeight(18);
          },
        );

        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
               // this.props.getCategories();
               this.getCategories();
            }else{
                this.setState({noConnection:'Strings.noConnection'})
            }
          });
      }


    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    getCategories = () => {
        this.setState({categoryLoading:true});
        axios.get(`${BASE_END_POINT}/categories`)
        .then(response=>{
            console.log('get catergory Done')
            console.log(response.data.data)
            this.setState({categoryLoading:false, categories:response.data.data,selectedCategory:response.data.data[0].id})
            this.getCategoryProducts(response.data.data[0].id,1,false);
        })
        .catch(error=>{
            this.setState({categoryLoading:false});
            console.log('error')
            console.log(error.response)
            if(!error.response){
               // dispatch({type:CATEGORY_FAILD,payload:Strigs.noConnection})
            }
      
        })
}

getCategoryProducts(categoryID, page, refresh) {
    this.setState({loading:true,flag:0})
    let uri = `${BASE_END_POINT}products/?categoryId=${categoryID}&page=${page}&limit=10`
    if (refresh) {
        this.setState({loading: false, refresh: true})
    } else {
        this.setState({refresh:false,loading:true})
    }
    axios.get(uri)
        .then(response => {
            console.log("products under category")
            console.log(response.data)
            this.setState({
                loading:false,
                refresh:false,
                pages:response.data.pageCount,
                noConnection:null,
                products: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.products.getAllData(), ...response.data.data]),
            })
            if(page==1){
                if(response.data.data.length==0){
                    RNToasty.Info({title:strings.noProducts})
                }
            }
        }).catch(error => {
            this.setState({loading:false,refresh:false})
            console.log(error.response);
            if (!error.response) {
                this.setState({errorText:strings.noConnection})
            }
        })

}
    
    
    componentDidMount(){
        console.log('home user   '+this.props.currentUser)
        this.enableDrawer();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})
                    this.getCategories();
                }
            }
          );
        
    }


    renderRow = (type, data, row) => {
        console.log('data')
        console.log(data)
        return (
            <HomeProductCard
             onPress={()=>{
                 this.props.navigator.push({
                     screen: 'ProductDetailsForHome',
                     animated: true,
                     passProps:{data:data}
                 })
             }}
             data={data} />
        );
       }

       renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }


   
  

    
   leftComponent = () => (
    <View style={{flexDirection:this.props.isRTL?'row-reverse':'row'}}>
        <TouchableOpacity
        onPress={()=>{
            if(this.props.currentUser){
                this.props.navigator.push({
                    screen:'Notifications',
                    animated:true,
                  })
            }else{
                RNToasty.Warn({title:strings.checkUser}) 
            }
        }}
         style={{marginHorizontal:moderateScale(7)}}>
            <Icon name='bell' color='white' size={20} />
        </TouchableOpacity>
        
    </View>
)

    renderCategoriesList = () => (
        <FlatList
         data={this.state.categories}
         renderItem={({item})=>{
             console.log('item')
             console.log(item)
             return(
            <TouchableOpacity
             onPress={()=>{
                 if(this.state.selectedCategory!=item.id){
                    this.setState({selectedCategory:item.id,products:new DataProvider()})
                    this.getCategoryProducts(item.id,1,false)
                 }
             }}
             style={{ marginTop:2, backgroundColor:'#e8e5e5', justifyContent:'center',alignItems:'center', width:responsiveWidth(16),height:responsiveHeight(12)}}>
                <FastImage
                 resizeMode='contain'
                 source={{uri:item.img}}
                 style={{width:responsiveWidth(13),height:responsiveHeight(7)}}
                 />
                <View style={{justifyContent:'center',alignItems:'center', width:responsiveWidth(14)}}>
                    <AppText textAlign='center' color='black' text={item.categoryname} />
                </View>
                
            </TouchableOpacity> 
             )
         }}
        />
    )

    renderProductList = () => (
        <RecyclerListView
            //scrollViewProps={vi}
            layoutProvider={this.renderLayoutProvider}
            dataProvider={this.state.flag>0 ? this.state.searchProducts : this.state.products}
            rowRenderer={this.renderRow}
            renderFooter={this.renderFooter}
            onEndReached={() => {

                if (this.page <= this.props.pages) {
                    this.page++;
                    this.getCategoryProducts(this.state.selectedCategory, this.page, false);
                }

            }}
            refreshControl={<RefreshControl colors={["#B7ED03"]}
                refreshing={this.state.refresh}
                onRefresh={() => {
                    this.page = 1
                    this.getCategoryProducts(this.state.selectedCategory, this.page, true);
                }
                } />}
            onEndReachedThreshold={.5}

        />

    )
    renderAppHeaderMenu = () => (
        <View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row', height: responsiveHeight(6), backgroundColor: 'white', width: responsiveWidth(70), borderRadius: moderateScale(10), alignItems: 'center', flex: 1 }}>
            <TextInput
        
                onChangeText={(value) => {
                    //console.log(this.state.products._data)
                    //Alert.alert('dd')
                    this.setState({flag:value.length})
                    this.list = this.state.products._data.filter((product)=>{
                      if(value.length>0){
                        if(product.name.toLowerCase().includes(value.toLocaleLowerCase())){
                            console.log('include')
                            return product
                        }
                      }
                    })
                    console.log(this.list.length)
                    this.setState({
                        searchProducts: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.list)
                    })
                       /* if(val.name.toLowerCase().includes(value.toLowerCase()) || val.name.toUpperCase().includes(value.toUpperCase()))
                            return val;
                    })
                    console.log(value)
                    console.log(this.list)
                    this.setState({
                        searchProducts: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.list)
                    })*/
                    //setTimeout(()=>console.log(this.state.searchProducts._size),1000)
                }}
                underlineColorAndroid='transparent'
                style={{textAlign:this.props.isRTL?"right":'left', marginHorizontal: 8, width: responsiveWidth(55) }}
                placeholder={strings.search}
            />
            <Icon style={{ fontSize: 15, color: 'gray' }} name='search' type='FontAwesome' />
        </View>
    )


    render(){
        const {isRTL} = this.props;
        return(
            <View style={{ flex:1}}>
                <AppHeader menu={()=>this.renderAppHeaderMenu()} showBurger leftComponent={this.leftComponent()} navigator={this.props.navigator} />            

                {this.state.categoryLoading?
                <View style={{ flex:1,justifyContent:'center',alignItems:'center'}}>
                    <LottieView
                        source={require('../assets/animations/update.json')}
                        autoPlay
                        loop
                        />
                </View>
                :
                <View style={{flex:1,flexDirection:isRTL?'row-reverse':'row'}}>
                    <View style={{width:responsiveWidth(18)}}>
                        {this.renderCategoriesList()}
                    </View>
                    {this.renderProductList()}
                </View>
                }

                <ActionButton
                    onPress={() => {
                        this.props.navigator.push({
                            screen: 'AddNewProduct',
                            animated: true
                        })
                    }}
                    active={this.state.fabActive}
                    hideShadow={true}
                    position={isRTL ? 'left' : 'right'}
                    buttonColor={colors.darkPrimaryColor}
                    renderIcon={() => <Icon color='white' size={20} name='plus' />}
                    renderIcon={() => <Icon color='white' size={20} name='plus' />}
                /> 

                <SnackBar
                    visible={this.state.noConnection!=null?true:false} 
                    textMessage={this.state.noConnection}
                    messageColor='white'
                    backgroundColor={colors.primaryColor}
                    autoHidingTime={5000}
                    />
                
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser, 
    categories: state.category.categories,    

})

const mapDispatchToProps = {
    getCategories,
}

export default connect(mapToStateProps,mapDispatchToProps)(Home);