import React,{Component} from 'react';
import {
    ActivityIndicator,TextInput, View,RefreshControl,TouchableOpacity,
    ScrollView,Platform,PermissionsAndroid
} from 'react-native';
import Permissions from 'react-native-permissions';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import { Button, Icon,Badge,Input,Item,Label } from 'native-base';
import Swiper from 'react-native-swiper';
import {AddProductToBacket} from '../actions/OrderAction';
import { RNToasty } from 'react-native-toasty';
import CommentsCard from '../components/CommentsCard';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../components/ListFooter';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import strings from '../assets/strings';

const MyButton =  withPreventDoubleClick(Button);


class ProductDetails extends Component {
  page = 1;
  state = {
    orderLoading:false,
    errorText: null,
    comments: new DataProvider(),
    refresh: false,
    loading: false,
    loading: false,
    pages: null,
    commentValue: "",
    price: "",
    enterPriceflag:false,
    showCommentDialog: false,
    showOrderDialog: false,
    orders:{},
    orderflag: 0,
  };

  static navigatorStyle = {
    navBarHidden: true,
    statusBarColor: colors.darkPrimaryColor
  };

  constructor(props) {
    super(props);
    this.renderLayoutProvider = new LayoutProvider(
      () => 1,
      (type, dim) => {
        dim.width = responsiveWidth(100);
        dim.height = responsiveHeight(10.2);
      }
    );
  }

  permissions = () => {
    Permissions.check('location').then(response => {
        if (response === 'denied' || response === 'undetermined') {
            this._requestPermission()
        } else {
            console.log('permission done')
            this.getLocation();
        }
    });
}

_requestPermission = () => {
    Permissions.request('location').then(response => {
        if (response === 'denied' || response === 'undetermined' ) {
            this._requestPermission()
        } else {
            console.log('permission done')
            this.getLocation();
        }
    })
}

getLocation = () => {
    this.watchID = navigator.geolocation.watchPosition((position) => {
        // Create the object to update this.state.mapRegion through the onRegionChange function
        let region = {
          latitude:       position.coords.latitude,
          longitude:      position.coords.longitude,
          latitudeDelta:  0.00922*1.5,
          longitudeDelta: 0.00421*1.5
        }

        console.log('region is 222  =>  ')
        console.log(region)
        this.setState({
            orders:{
                ...this.state.orders,
                destination:[position.coords.latitude,position.coords.longitude]
            }
        })
       // this.props.userLocation([position.coords.latitude,position.coords.longitude])
       
       // this.onRegionChange(region, region.latitude, region.longitude);
      }, (error)=>console.log(error));
}

checkPermissionForApiLess23 = async () => {
    try{
        const p = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if(p != PermissionsAndroid.RESULTS.GRANTED){
            this.putPermissionForApiLess23()
        }else{
           console.log('pppppppppprrrrr')
           this.getLocation()             
        }
    }catch (err) {
        console.log(err)
      }
}
putPermissionForApiLess23 = async () => {
    try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            this.getLocation()
        } else {
            this.putPermissionForApiLess23()
        }

        
      } catch (err) {
        console.log(err)
      }
}


  renderRow = (type, data, row) => {
    return <CommentsCard data={data} />;
  };

  renderFooter = () => {
    return this.state.loading ? (
      <View style={{ alignSelf: "center", margin: moderateScale(5) }}>
        <ListFooter />
      </View>
    ) : null;
  };

  getComments(page, refresh) {
    this.setState({ loading: true });
    let uri = `${BASE_END_POINT}comments?productId=${
      this.props.data.product.id
    }&userId=${this.props.currentUser.user.id}&page=${page}&limit=10`;
    if (refresh) {
      this.setState({ loading: false, refresh: true });
    } else {
      this.setState({ refresh: false, loading: true });
    }
    axios
      .get(uri)
      .then(response => {
        this.setState({
          loading: false,
          refresh: false,
          pages: response.data.pageCount,
          errorText: null,
          comments: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(
            this.state.refresh
              ? [...response.data.data]
              : [...this.state.comments.getAllData(), ...response.data.data]
          )
        });
        console.log("DONE DONE ");
        console.log(response.data.data);
      })
      .catch(error => {
        this.setState({ loading: false, refresh: false });
        console.log(error.response);
        if (!error.response) {
          this.setState({ errorText: Strings.noConnection });
        }
      });
  }

  componentDidMount() {
    this.enableDrawer();
    this.getComments(1, false);
    if(Platform.Version < 23){
        this.checkPermissionForApiLess23()
    }else{
        this.permissions();
    }
  }

  enableDrawer = () => {
    this.props.navigator.setDrawerEnabled({
      side: "left",
      enabled: false
    });
    this.props.navigator.setDrawerEnabled({
      side: "right",
      enabled: false
    });
  };

  renderProductImages = () => (
    <Swiper
      autoplay={true}
      loop={true}
      activeDotColor="black"
      dotColor="gray"
      paginationStyle={{ bottom: 0, width: 100 }}
    >
      {this.props.data.product.img.map(img => (
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            width: responsiveWidth(100),
            height: responsiveHeight(25),
            backgroundColor: "white",
            elevation: 3
          }}
        >
          <FastImage
            resizeMode="contain"
            style={{ width: responsiveWidth(50), height: responsiveHeight(20) }}
            source={{ uri: img }}
          />
        </View>
      ))}
    </Swiper>
  );

  renderOrderdButton() {
    //const { handleSubmit } = this.props;
    return (
      <MyButton
        onPress={()=>{
            this.setState({showOrderDialog:true})
        }}
        style={{
          marginTop: moderateScale(20),
          alignSelf: "center",
          justifyContent: "center",
          alignItems: "center",
          height: responsiveHeight(7),
          width: responsiveWidth(50),
          backgroundColor: colors.darkPrimaryColor,
          borderRadius: moderateScale(10)
        }}
      >
      <AppText
            fontSize={responsiveFontSize(3)}
            text={Strings.order}
            color="white"
          />
       
      </MyButton>
    );
  }

  renderSendCommentButton() {
    //const { handleSubmit } = this.props;
    return (
      <MyButton
        onPress={()=>{
            if(this.state.price.length==0||this.state.commentValue.length==0){
                this.setState({enterPriceflag:true})
                console.log(':) :) :) '+Strings.priceFlag)
            }else{
                console.log(this.state.price)
                console.log(this.state.commentValue)
                this.setState({showCommentDialog:false}) 
                this.sendComment()         
            }
        }}
        style={{
          marginTop: moderateScale(5),
          alignSelf: "center",
          justifyContent: "center",
          alignItems: "center",
          height: responsiveHeight(7),
          width: responsiveWidth(40),
          backgroundColor: colors.darkPrimaryColor,
          borderRadius: moderateScale(10)
        }}
      >
        <AppText
          fontSize={responsiveFontSize(3)}
          text={Strings.send}
          color="white"
        />
      </MyButton>
    );
  }

  renderSendOrderButton() {
    //const { handleSubmit } = this.props;
    return (
      <MyButton
        onPress={()=>{
            if(this.state.orderflag!=0){
                if(!this.state.orderLoading){
                    this.sendOrder()
                }
            }
        }}
        style={{
          marginTop: moderateScale(5),
          alignSelf: "center",
          justifyContent: "center",
          alignItems: "center",
          height: responsiveHeight(7),
          width: responsiveWidth(40),
          backgroundColor: colors.darkPrimaryColor,
          borderRadius: moderateScale(10)
        }}
      >
       {this.state.orderLoading ? (
          <ActivityIndicator color="white" />
        ) : (
            <AppText
            fontSize={responsiveFontSize(3)}
            text={Strings.send}
            color="white"
          />
        )}       
      </MyButton>
    );
  }

  renderComments = () => {
    const { isRTL } = this.props;
    return (
      <View
        style={{
          marginTop: moderateScale(8),
          alignSelf: "center",
          width: responsiveWidth(100)
        }}
      >
        <View
          style={{
            marginBottom: moderateScale(2),
            alignSelf: "center",
            width: responsiveWidth(92),
            alignItems: "center",
            flexDirection: isRTL ? "row-reverse" : "row",
            justifyContent: "space-between"
          }}
        >
          <View style={{ flexDirection: isRTL ? "row-reverse" : "row" }}>
            <AppText
              text={Strings.comments}
              fontWeight="400"
              color={colors.buttonColor}
              fontSize={responsiveFontSize(2.7)}
            />
            <Badge
              success
              style={{
                elevation: 2,
                height: 23,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <AppText
                color="white"
                text={this.state.comments._size}
              />
            </Badge>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.setState({ showCommentDialog: true });
            }}
          >
            <Icon
              type="FontAwesome"
              name="plus"
              style={{ color: colors.primaryColor, fontSize: 17 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  sendComment = () => {
    axios.post(`${BASE_END_POINT}comments/${this.props.data.product.id}/products`, JSON.stringify({
        comment: this.state.commentValue,
        price: this.state.price,
      }), {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
        },
    }).then(response => {
      console.log('Add Comment Success')
      console.log(response)
      this.setState({price:'',commentValue:''}) 
      this.getComments(this.page, true);
    }).catch(error=>{
         this.setState({price:'',commentValue:''})
        console.log(error.response)
        if (!error.response) {
            RNToasty.Error({title: Strings.noConnection})
            
          }
    });
  }
//
  sendOrder = () => {
    axios.post(`${BASE_END_POINT}orders/${this.props.data.user.id}/users`, JSON.stringify(this.state.orders),
     {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
        },
    }).then(response => {
      console.log('send Order Success')
      console.log(response)
      RNToasty.Success({title:Strings.orderSuccess})
      this.setState({showOrderDialog:false, orderLoading:false,orderflag:0}) 
    }).catch(error=>{
        this.setState({showOrderDialog:false, orderLoading:false,orderflag:0}) 
        console.log(error.response)
        if (!error.response) {
            RNToasty.Error({title: Strings.noConnection})
            
          }
    });
  }


  renderCommentDialog = () => (
    <Dialog
    onTouchOutside={()=>{
        this.setState({enterPriceflag:false,showCommentDialog:false})
    }}
      width={responsiveWidth(80)}
      height={responsiveHeight(48)}
      visible={this.state.showCommentDialog}
      dialogTitle={<DialogTitle title={Strings.addComment} />}
    >
     
        <View
          style={{
            width: responsiveWidth(70),
            alignSelf: "center",
            marginTop: moderateScale(8)
          }}
        >
          <TextInput
            onChangeText={value => {
              this.setState({ commentValue: value });
            }}
            placeholder={Strings.addComment}
            style={{textAlign:this.props.isRTL?'right':'left',  width: responsiveWidth(70) }}
          />

          <View
            style={{
              marginTop: moderateScale(5),
              alignSelf: this.props.isRTL ? "flex-end" : "flex-start"
            }}
          >
            <AppText
              text={Strings.price}
              color={colors.darkPrimaryColor}
              fontWeight="400"
              fontSize={responsiveFontSize(2.5)}
            />
            <TextInput
              keyboardType="numeric"
              onChangeText={value => {
                this.setState({ price: value,enterPriceflag:false });
              }}
              style={{textAlign:this.props.isRTL?'right':'left' , width: responsiveWidth(70) }}
            />
            
          </View>

          {this.state.enterPriceflag&&
          <View style={{alignSelf:this.props.isRTL?'flex-start':'flex-end',marginVertical:moderateScale(0.5)}}>
            <AppText text={strings.require} color='red' />
          </View>
          }

          {this.renderSendCommentButton()}
        </View>
      
    </Dialog>
  );

  renderOrderDialog = () => (
    <Dialog
    onTouchOutside={()=>{
        this.setState({showOrderDialog:false})
    }}
      width={responsiveWidth(80)}
      height={responsiveHeight(35)}
      visible={this.state.showOrderDialog}
      dialogTitle={<DialogTitle title={Strings.makeOrder} />}
    >
     
        <View
          style={{
            width: responsiveWidth(70),
            alignSelf: "center",
            marginTop: moderateScale(3)
          }}
        >
       

          <View
            style={{
              marginTop: moderateScale(5),
              alignSelf: this.props.isRTL ? "flex-end" : "flex-start"
            }}
          >
            <AppText
              text={Strings.count}
              color={colors.darkPrimaryColor}
              fontWeight="400"
              fontSize={responsiveFontSize(2.5)}
            />
            <TextInput
              keyboardType="numeric"
              onChangeText={value => {
            const o = {
                product:this.props.data.product.id,
                count: Number(value)
            }
                this.setState({
                    orders:{
                        ...this.state.orders,
                        productOrders:[o]
                    },
                    orderflag:1
                });
              }}
              style={{textAlign:this.props.isRTL?'right':'left' , width: responsiveWidth(70) }}
            />
            
          </View>

          {this.renderSendOrderButton()}
          
        </View>
      
    </Dialog>
  );

  render() {
    const { data, isRTL,fromHome } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            elevation: 3,
            width: responsiveWidth(100),
            height: responsiveHeight(25)
          }}
        >
          {this.renderProductImages()}
        </View>

        <View
          style={{
            marginTop: moderateScale(2),
            elevation: 2,
            backgroundColor: "#6d5151",
            width: responsiveWidth(100),
            height: responsiveHeight(9),
            justifyContent: "center"
          }}
        >
          <View
            style={{
              alignSelf: isRTL ? "flex-end" : "flex-start",
              marginVertical: moderateScale(3),
              marginHorizontal: moderateScale(6)
            }}
          >
            <AppText
              text={data.product.name}
              fontSize={responsiveFontSize(2.7)}
              fontWeight="400"
              color="white"
            />
            <AppText
              text={
                data.product.advertisingType != "auction"
                  ? data.product.price
                  : "auction"
              }
              fontSize={responsiveFontSize(2.3)}
              color="white"
            />
          </View>
        </View>

        <View
          style={{
            elevation: 2,
            backgroundColor: "#f2efef",
            width: responsiveWidth(100),
            height: responsiveHeight(7),
            justifyContent: "center"
          }}
        >
          <View
            style={{
              alignItems: "center",
              marginHorizontal: moderateScale(6),
              flexDirection: isRTL ? "row-reverse" : "row",
              justifyContent: "space-between"
            }}
          >
            <View style={{ flexDirection: isRTL ? "row-reverse" : "row" }}>
              <AppText
                text={Strings.seller}
                fontSize={responsiveFontSize(2.5)}
                fontWeight="400"
                color={colors.buttonColor}
              />
              <AppText
                text=" : "
                fontSize={responsiveFontSize(2.5)}
                fontWeight="400"
                color={colors.buttonColor}
              />
              <AppText
                text={`${data.user.firstname} ${data.user.lastname}`}
                fontSize={responsiveFontSize(2.5)}
              />
            </View>
            <TouchableOpacity>
              <Icon
                type="MaterialIcons"
                name="phone-in-talk"
                style={{ color: colors.primaryColor }}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            alignSelf: isRTL ? "flex-end" : "flex-start",
            marginTop: moderateScale(8),
            marginHorizontal: moderateScale(5)
          }}
        >
          <AppText
            text={`${Strings.details} : `}
            fontWeight="400"
            color={colors.buttonColor}
            fontSize={responsiveFontSize(2.7)}
          />
          <AppText
            text={data.product.description}
            color="gray"
            fontSize={responsiveFontSize(2.4)}
          />
        </View>

        <View
          style={{
            alignSelf: isRTL ? "flex-end" : "flex-start",
            marginTop: moderateScale(5),
            marginHorizontal: moderateScale(5)
          }}
        >
          <AppText
            text={`${Strings.specifications} : `}
            fontWeight="400"
            color={colors.buttonColor}
            fontSize={responsiveFontSize(2.7)}
          />
          {data.product.specifications.map(data => (
            <AppText
              text={` - ${data}`}
              color="gray"
              fontSize={responsiveFontSize(2.4)}
            />
          ))}
        </View>

        {data.product.advertisingType != "auction"
          ? data.user.id != this.props.currentUser.user.id && this.renderOrderdButton()
          : this.renderComments()
        }
        <RecyclerListView
          layoutProvider={this.renderLayoutProvider}
          dataProvider={this.state.comments}
          rowRenderer={this.renderRow}
          renderFooter={this.renderFooter}
          onEndReached={() => {
            if (this.page <= this.props.pages) {
              this.page++;
              this.getComments(this.page, false);
            }
          }}
          onEndReachedThreshold={0.5}
        />

        {this.renderCommentDialog()}
        {this.renderOrderDialog()}
      </View>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
  
}

export default connect(mapToStateProps,mapDispatchToProps)(ProductDetails);