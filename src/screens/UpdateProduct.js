import React,{Component} from 'react';
import {NetInfo,Platform, View,TouchableOpacity,Image,ScrollView,Keyboard} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import { Button, Icon,Item,Label,Picker, Card } from 'native-base';
import {AddProductToBacket} from '../actions/OrderAction';
import {getOwnerProducts} from '../actions/ProductAction';
import { RNToasty } from 'react-native-toasty';
import AppSpinner from '../common/AppSpinner';
import ImagePicker from 'react-native-image-crop-picker';
import AppInput from '../common/AppInput';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import SnackBar from 'react-native-snackbar-component';
import Dialog, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import TagInput from 'react-native-tag-input';
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);




const validate = values => {
    const errors = {};
    if (!values.productName) {
        errors.productName = Strings.require;
    }
    
    if (!values.description) {
        errors.description = Strings.require;
    }

    /*if (!values.specifications) {
        errors.specifications = Strings.require;
    }*/

    if (!values.price) {
        errors.price = Strings.require;
    }

    return errors;
};

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

const inputProps = {
    keyboardType: 'default',
    placeholder: 'add new',
    autoFocus: false,
    style: {
      fontSize: 14,
      marginVertical: Platform.OS == 'ios' ? 10 : -2,
      justifyContent:'center',
    },
  };


class UpdateProduct extends Component{

    
    state = {
        showDialog:false,
        currentImage:null,
        indexOfcurrentImage:null,
        tags: this.props.currentProduct.specifications,
        text: "",
        
        uploadProduct:false,
        productImages:this.props.currentProduct.img,
        noConnection:null,
        categories:[],
        selectedCategory: null,
        //selectedPartCategory:this.props.categories[0].id,
        selectedProductType:this.props.currentProduct.advertisingType,
    }

    constructor(props){
        super(props);
        console.log("Images ")
        console.log(this.props.currentProduct.img)
        console.log(this.state.productImages)
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getCategories()
            }else{
                this.setState({noConnection:Strings.noConnection})
            }
          });
    }

     getCategories = () => {
             axios.get(`${BASE_END_POINT}/categories`)
             .then(response=>{
                 console.log('get catergory Done')
                 this.setState({categories:response.data.data,selectedCategory:this.props.currentProduct.category[0].id})
                
             })
             .catch(error=>{
                 console.log('error')
                 console.log(error.response)
                 if(!error.response){
                    // dispatch({type:CATEGORY_FAILD,payload:Strigs.noConnection})
                 }
           
             })
     }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
    
    componentDidMount(){
        this.enableDrawer()
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})
                    this.getCategories()
                }
            }
          );   
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

  
    renderProductImages = () => {
        return(
            <View style={{width:responsiveWidth(90),alignSelf:'center',marginTop:moderateScale(5)}}>
                <View>
                    <AppText text={Strings.prodctPhotos} color='black' fontSize={responsiveFontSize(2.5)} />
                </View>
                <View style={{marginTop:moderateScale(8),flexDirection:this.props.isRTL?'row-reverse':'row' }}>
                    {this.state.productImages.map((img,index) => {
                        return (
                            <TouchableOpacity
                                style={{marginHorizontal:moderateScale(2)}}
                                onPress={() => {
                                    this.setState({indexOfcurrentImage:index, currentImage:img,showDialog:true})
                                }}
                            >
                                <Image
                                    resizeMode='contain'
                                    source={{ uri: img }}
                                    style={{ borderWidth: 1.5, borderColor: '#CCCCCC', height: responsiveHeight(17), width: responsiveWidth(21), borderRadius: moderateScale(2) }} />
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>
        )
    }
    /*
    */

    onUpdateProdut(values) {
        console.log("product id ")
        console.log(this.state.selectedCategory);
        if(this.state.productImages.length>0){
            this.setState({uploadProduct:true});
            var data = new FormData();
            let count = 0;
            
            data.append('name',values.productName);
            data.append('price',values.price)
            data.append('description',values.description)
            data.append('advertisingType',this.state.selectedProductType)
            data.append('category',this.state.selectedCategory)
            //data.append('img','my img')
            for(i=0;i<this.state.tags.length;i++){
                data.append('specifications',this.state.tags[i])
            }
            /*for(i=0;i<this.state.productImages.length;i++){
                console.log('img    ',this.state.productImages[i])
                data.append('img',{
                    uri: this.state.productImages[i],
                    type: 'multipart/form-data',
                    name: 'productImages'
                })
            }*/
            this.state.productImages.filter(img=>{
                if(count<this.state.productImages.length){
                    count++;
                    data.append('img',{
                        uri: img,
                        type: 'multipart/form-data',
                        name: 'productImages'
                    }) 
                }
            })

            data.append('quantity',1)
            
            
            console.log(data);
            console.log(`${BASE_END_POINT}products/${this.props.currentProduct.id}`)
            axios.put(`${BASE_END_POINT}products/${this.props.currentProduct.id}`, data, {
                headers: {
                  'Authorization': `Bearer ${this.props.currentUser.token}`
                },
              })
            .then(response=>{
                console.log('product Updated')
                console.log(response.data);       
                this.setState({uploadProduct:false})
                RNToasty.Success({title:Strings.updateProductSuccess}) 
                /*this.props.navigator. popTo({
                    screen: 'CustomerProfile',
                    animated: false,
                  });*/
                this.props.navigator.push({
                    screen: 'CustomerProfile',
                    animated: true,
                })               
            }).catch(error=>{
                console.log(error);
                console.log(error.message);
                console.log(error.status);
                console.log(error.response);
                this.setState({uploadProduct:false})
            })      
            
        }else{
            RNToasty.Warn({title:Strings.pleaseSelectProductImages})
        }
    }

    makeImageMain() {   
            axios.put(`${BASE_END_POINT}products/${this.props.currentProduct.id}/mainImg`, JSON.stringify({index:this.state.indexOfcurrentImage}), {
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${this.props.currentUser.token}`
                },
              })
            .then(response=>{
                console.log('product Updated')
                console.log(response);  
                this.setState({showDialog:false})     
                RNToasty.Success({title:Strings.done})  
            }).catch(error=>{
                console.log(error)
                console.log(error.response)
                if(!error.response){

                }
            })      
        }    

    renderSaveButtons() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={
                handleSubmit(this.onUpdateProdut.bind(this)) 
            }
            style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(7),width:responsiveWidth(30),backgroundColor:colors.darkPrimaryColor,borderRadius:moderateScale(10)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.save} color='white' />
            </MyButton>
        );
    }
    renderCloseButtons() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={()=>{
                this.props.navigator.pop()
            }}
            style={{marginHorizontal:moderateScale(5), justifyContent:'center',alignItems:'center',height:responsiveHeight(7),width:responsiveWidth(30),backgroundColor:colors.buttonColor,borderRadius:moderateScale(10)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.cancel} color='white' />
            </MyButton>
        );
    }
    renderMainButton() {
        return (
            <MyButton 
            onPress={()=>{this.makeImageMain()}}
            style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(6),width:responsiveWidth(33),backgroundColor:colors.darkPrimaryColor,borderRadius:moderateScale(10)}}
            >
                <AppText fontSize={responsiveFontSize(2.5)} text={Strings.makePhotoMain} color='white' />
            </MyButton>
        );
    }

    renderDeleteButton() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={()=>{
                const images = this.state.productImages.filter(img=>{
                    if(this.state.currentImage!=img){
                        return img
                    }
                })
                console.log(images)
                this.setState({productImages:images,showDialog:false})
            }}
            style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(6),width:responsiveWidth(30),backgroundColor:'red',borderRadius:moderateScale(10),marginHorizontal:moderateScale(4)}}
            >
                <AppText fontSize={responsiveFontSize(2.5)} text={Strings.delete} color='white' />
            </MyButton>
        );
    }

    renderCategoryPicker = () => {
        return(
            <Item style={{marginTop:moderateScale(3), borderBottomColor:'gray', marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
                <Label style={{fontSize:responsiveFontSize(2.1),color:'gray',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.category}</Label>
            </View>
             <View style={{flex:1}}>
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedCategory}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedCategory:value});
                     }}
                     >
                     {this.state.categories.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.categoryname} value={obj.id} />
                     ))}
                    
                 </Picker>
                 
             </View>
         </Item>
        )
    }

    renderImageDialog = () => (
        <Dialog
        onTouchOutside={()=>{
            this.setState({showDialog:false})
        }}
          width={responsiveWidth(80)}
          height={responsiveHeight(44)}
          visible={this.state.showDialog}
          dialogTitle={<DialogTitle title={Strings.productMainImage} />}
        >
         
         <Image
          source={{uri:this.state.currentImage}}
          style={{alignSelf:'center',width:responsiveWidth(60),height:responsiveHeight(20),marginTop:moderateScale(5)}}
         />

        <View style={{marginVertical:moderateScale(8), alignSelf:'center',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            {this.renderMainButton()}
            {this.renderDeleteButton()}
        </View>

        </Dialog>
      );
   

    renderProductTypePicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(7), marginBottom:moderateScale(3),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
                <Label style={{fontSize:responsiveFontSize(2.1),color:'gray',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.productType}</Label>
            </View>
             <View style={{flex:1}}>
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedProductType}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedProductType:value});
                     }}
                     >
                     <Picker.Item key='price' label={Strings.price} value='price' />
                     <Picker.Item key='auction' label={Strings.auction} value='auction' />
                    
        
                 </Picker>
             </View>
         </Item>
        )
    }

    onChangeTags = (tags) => {
        this.setState({ tags });
      }
    
      onChangeText = (text) => {
        this.setState({ text });
    
        const lastTyped = text.charAt(text.length - 1);
        const parseWhen = [',', ';','.'];
    
        if (parseWhen.indexOf(lastTyped) > -1) {
          this.setState({
            tags: [...this.state.tags, this.state.text],
            text: "",
          });
          console.log(this.state.tags)
        }

        
      }
    
      labelExtractor = (tag) => tag;
    
    renderFileds() {
        // const { navigator, isRTL } = this.props;
         return (
             <View>
 
                {this.renderProductTypePicker()}

                 <Field borderColor='gray' style={{ width: responsiveWidth(90) }} textColor={colors.primaryColor} name="productName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.productName} component={InputComponent} returnKeyType="done"
                     onSubmit={() => {
                         Keyboard.dismiss()
                     }}
                 />

                 <Field  borderColor='gray' style={{ width: responsiveWidth(90) }} textColor={colors.primaryColor} name="description" isRTL={this.props.isRTL}  marginBottom={moderateScale(15)} label={Strings.description} component={InputComponent} returnKeyType="done"
                     onSubmit={() => {
                        Keyboard.dismiss()
                     }}
                 />

                  {this.renderCategoryPicker()}
                  
                  <View style={{borderBottomWidth:1,borderBottomColor:'gray' ,marginBottom:moderateScale(5), marginTop:moderateScale(15), width:responsiveWidth(90),alignSelf:'center'}}>
                     <View style={{marginBottom:moderateScale(7), alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
                         <AppText text={`${Strings.specifications} *`} color='gray' fontSize={responsiveFontSize(2.2)} />
                     </View>
                     <TagInput
                            value={this.state.tags}
                            onChange={this.onChangeTags}
                            labelExtractor={this.labelExtractor}
                            text={this.state.text}
                            onChangeText={this.onChangeText}
                            maxHeight={responsiveHeight(10)}
                            inputProps={inputProps}
                        />
                  </View>

                {/*  <Field  borderColor='gray' style={{ width: responsiveWidth(90) }} textColor={colors.primaryColor}  name="specifications" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.specifications} component={InputComponent} returnKeyType="done"
                     onSubmit={() => {
                        Keyboard.dismiss()
                     }}
                 />
                */}
                 <Field numeric borderColor='gray' style={{ width: responsiveWidth(90) }}  numeric name="price" isRTL={this.props.isRTL}  marginBottom={moderateScale(6)} label={Strings.price} component={InputComponent} returnKeyType="done"
                     onSubmit={() => {
                        Keyboard.dismiss()
                     }}
                 />

             </View>
 
         )
     }

     renderNoConnection = () => (
        <SnackBar
        visible={this.state.noConnection!=null?true:false} 
        textMessage={Strings.noConnection}
        messageColor='white'
        backgroundColor={colors.primaryColor}
        autoHidingTime={5000}
        />
     )

    render(){
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={this.props.navigator} showBack title={Strings.updateProduct} />
                <ScrollView> 
                    <Card style={{width:responsiveWidth(96),alignSelf:'center'}}>
                        <View style={{ alignSelf: 'center', width: responsiveWidth(90) }}>
                        {this.renderFileds()}
                        </View>
                    </Card>

                    <Card style={{marginTop:moderateScale(1), width:responsiveWidth(96),alignSelf:'center'}}>
                        {this.renderProductImages()}
                        <View style={{flexDirection:this.props.isRTL?'row-reverse':'row', justifyContent:'center',alignItems:'center',alignSelf:'center',marginVertical:moderateScale(8)}}>
                            {this.renderSaveButtons()}
                            {this.renderCloseButtons()}
                        </View>
                    </Card>
                   {this.state.uploadProduct&&<LoadingDialogOverlay title={Strings.waitUpdateProduct} />}
                    {this.renderImageDialog()}
                </ScrollView>
                {/*  <ScrollView>
                    {this.renderUploadProductImagesPart()}
                    
                    {this.renderAddButton()}
                </ScrollView> 
                {this.renderNoConnection()}
                {this.state.uploadProduct&&<LoadingOverlay/>}  
            */}      
            </View>          
        )
    }
}

const form = reduxForm({
    form: "UpdateProduct",
    enableReinitialize:true,
    validate,
})(UpdateProduct)


const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    currentProduct: state.product.currentProduct,
    initialValues: {
        productName: state.product.currentProduct.name,
        description: state.product.currentProduct.description,
        price: state.product.currentProduct.price,
      },
})

const mapDispatchToProps = {
    AddProductToBacket,
    getOwnerProducts,
}

export default connect(mapToStateProps,mapDispatchToProps)(form);