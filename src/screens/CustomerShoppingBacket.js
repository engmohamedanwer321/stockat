import React,{Component} from 'react';
import {View,RefreshControl,StyleSheet,Alert} from 'react-native';
import { moderateScale, responsiveWidth,  responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Button} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import AppSpinner from '../common/AppSpinner';
import SnackBar from 'react-native-snackbar-component';
import ProductCard from '../components/ProductCard';
import LoadingOverlay from '../components/LoadingOverlay';
import {RemoveProductFromBacket,ClearBacket} from '../actions/OrderAction';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';

class CustomerShoppingBacket extends Component {

    state = {
        noConnection:null,
        orderLoading:false,
    }
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }

    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 2,
          (type, dim) => {
            dim.width = responsiveWidth(50);
            dim.height = responsiveHeight(33);
          },
        );
      }

    componentDidMount(){
        this.enableDrawer()
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    totalPrice = () => {
        const {orders,ordersData} = this.props;
        total = 0;
        for(var i=0; i<orders.productOrders.length; i++){
            total+= orders.productOrders[i].count*ordersData[i].price ;
        }
        return total;
    }
    renderRow = (type, data, row) => {
     return (
    <View style={{marginBottom:moderateScale(1), marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <ProductCard 
        removeProduct={()=>{
            this.props.RemoveProductFromBacket(data.id);
        }}
         remove={true}
         data={data}
        />
    </View> 
     );
    }

    sendOrder = () => {
        //let userID = this.props.currentUser.user.id;
        let userToken = this.props.currentUser.token;
        console.log(this.props.orders)
        this.setState({orderLoading:true})
        axios.post(`${BASE_END_POINT}users/${2}/orders`, JSON.stringify(this.props.orders), {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMCIsImlzcyI6ImJvb2RyQ2FyIiwiaWF0IjoxNTQwNjUxMjk1NTUzLCJleHAiOjE1NDA2NTE5MDAzNTN9.vf2XkQfewGwBOyU4d-Yo7K6nu2xoVmHUDh-Xl5_DCgc`    
            },
          }).then(response=>{
            console.log(response)
              RNToasty.Success({title:Strings.orderDone});
              this.props.ClearBacket()
              console.log(this.props.orders.productOrders)
              this.setState({orderLoading:false})
          }).catch(error=>{
           console.log(error.response)
            this.setState({orderLoading:false})
            if(!error.response){
                RNToasty.Error({title:Strings.noConnection});
            }
          });
    }
   
    render(){
        const {navigator,orders,ordersData} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.myCart} showBurger navigator={navigator}/>
                <RecyclerListView            
                    layoutProvider={this.renderLayoutProvider}
                    dataProvider={new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(ordersData)}
                    rowRenderer={this.renderRow}                  
                />

                {ordersData.length>0&&
                <View style={{marginBottom:moderateScale(20),justifyContent:'center',alignItems:'center',flexDirection:this.props.isRTL?'row-reverse':'row', height:responsiveHeight(8),backgroundColor:'#CCCCCC'}}>
                    <AppText paddingHorizontal={moderateScale(2)} text={Strings.totalPrice} color={colors.primaryColor} fontSize={responsiveFontSize(2)} />
                    <AppText paddingHorizontal={moderateScale(2)} text={this.totalPrice()} color={colors.primaryColor} fontSize={responsiveFontSize(3)} />
                    <AppText text={Strings.sar} color={colors.primaryColor} fontSize={responsiveFontSize(3)} />
                </View>
                }

                {ordersData.length>0&&
                    <Button
                    onPress={()=>{
                        if(this.props.orders.productOrders.length==0){
                            RNToasty.Info({title:Strings.mustMakeOrder})
                        }else{
                           this.sendOrder();
                        }
                        
                    }}
                    style={{marginTop:moderateScale(6), width:responsiveWidth(100), justifyContent:'center',alignItems:'center',position: 'absolute',bottom:0, backgroundColor:'#82C141'}}>
                       <AppText fontSize={responsiveFontSize(3)} color='white' text={Strings.sendOrder} />
                   </Button>
                }
                {this.state.orderLoading&&<LoadingOverlay/>}
                {this.props.logoutLoading&&<LoadingDialogOverlay title={Strings.waitLogout}/>}
                
            </View>
        );
    }
}



const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    orders: state.order.orders,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
    logoutLoading: state.menu.logoutLoading,
})

const mapDispatchToProps = {
    RemoveProductFromBacket,
    ClearBacket,
}

export default connect(mapStateToProps,mapDispatchToProps)(CustomerShoppingBacket);
