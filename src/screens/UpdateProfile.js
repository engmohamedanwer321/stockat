import React, { Component } from 'react';
import {
  View,StatusBar,Keyboard,ScrollView,AsyncStorage,TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Button,  Icon, Item, Picker, Label, Thumbnail } from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import AppSpinner from '../common/AppSpinner';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import Checkbox from 'react-native-custom-checkbox';
import { signup } from '../actions/SignupAction';
import {getUser,userToken} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import SnackBar from 'react-native-snackbar-component';
import ImagePicker from 'react-native-image-crop-picker';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);



const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }

    if (!values.firstName) {
        errors.firstName = Strings.require;
    } else if (!isNaN(values.firstName)) {
        errors.firstName = Strings.errorName;
    }

    if (!values.lastName) {
        errors.lastName = Strings.require;
    } else if (!isNaN(values.lastName)) {
        errors.lastName = Strings.errorName;
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = Strings.require;
    } else if (!values.phoneNumber.startsWith('05')) {
        errors.phoneNumber = Strings.errorStartPhone;
    }  else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = Strings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = Strings.errorPhoneFormat
    }

    if (!values.address) {
        errors.address = Strings.require;
    }

    
    return errors;
};
;

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class UpdateProfile extends Component {

    state = {
        userImage:null,
        noConnection:null,
        updateLoading:false,
        token:this.props.currentUser.token,
    }

    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator; 
    }
    
    componentDidMount(){    
        this.disableDrawer();
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    
    onUpdate(values) {
        console.log('values');
        console.log(values);
        var data = new FormData(); 
            data.append('firstname',values.firstName);
            data.append('lastname',values.lastName)
            data.append('phone',values.phoneNumber)
            data.append('email',values.email)
            data.append('address',values.address)
        if(this.state.userImage!=null){
            //userData.img = this.state.userImage
            data.append('img',{
                uri: this.state.userImage,
                type: 'multipart/form-data',
                name: 'productImages'
            }) 
        } 
        console.log('new user') 
        //console.log(userData)
       this.setState({updateLoading:true})
        axios.put(`${BASE_END_POINT}user/updateInfo`, data, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
            console.log('update user profile done')      
            console.log(response.data)
            this.setState({updateLoading:false,noConnection:null});           
            const user = {...this.props.currentUser,user:{...response.data.user}}
            console.log('current user')
            console.log(user)
            RNToasty.Success({title:Strings.updateProfileSuccess})
            this.props.getUser(user)
            AsyncStorage.setItem('@BoodyCarUser', JSON.stringify(user));
            this.props.navigator.push({
                screen: 'CustomerProfile',
                animated: true,
            })
        }).catch(error => {
            console.log('error');
            console.log(error.response);
            console.log(error);
            this.setState({updateLoading:false});
             
            if (!error.response) {
            this.setState({noConnection:Strings.noConnection});
            } 
          });
    }
    
    renderSaveButtons() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={
                handleSubmit(this.onUpdate.bind(this)) 
            }
            style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(7),width:responsiveWidth(30),backgroundColor:colors.darkPrimaryColor,borderRadius:moderateScale(10)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.save} color='white' />
            </MyButton>
        );
    }
    renderCloseButtons() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={()=>{
                this.props.navigator.pop()
            }}
            style={{marginHorizontal:moderateScale(5), justifyContent:'center',alignItems:'center',height:responsiveHeight(7),width:responsiveWidth(30),backgroundColor:colors.buttonColor,borderRadius:moderateScale(10)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.cancel} color='white' />
            </MyButton>
        );
    }

    
    renderAreaPicker = () => {
        console.log("rtl  "+ this.props.isRTL);
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(7), marginBottom:moderateScale(5),width:responsiveWidth(80),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
               <View style={{ flex:1}}>
               <Label style={{fontSize:responsiveFontSize(3),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.area}</Label>
               </View>
                <View style={{flex:1}}>
                    {
                        this.state.areaLoading?
                        <AppSpinner isRTL={this.props.isRTL} />
                        :
                        <Picker           
                        mode="dropdown"
                        iosHeader="Select your SIM"
                        iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                        selectedValue={this.state.selectedArea}
                        style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                        onValueChange={(value,index)=>{
                           // this.props.setArea(value);
                           this.setState({selectedArea:value});
                        }}
                        >
                    </Picker>
                    }
                </View>
            </Item>
        )
}
    
    
renderContent() {
    const { isRTL } = this.props;
    return (
        <View>

            <View style={{width:responsiveWidth(80), flexDirection:isRTL?'row-reverse':'row'}}>
                <View style={{width:responsiveWidth(39)}}>
                    <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} name="firstName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.firstName} component={InputComponent}
                    returnKeyType="done"
                        onSubmit={() => {
                            Keyboard.dismiss();
                        }}
                    />
                </View>

                <View style={{marginHorizontal:moderateScale(2), width:responsiveWidth(39)}}>
                    <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} name="lastName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.secondName} component={InputComponent} 
                        returnKeyType="done"
                        onSubmit={() => {
                            Keyboard.dismiss();
                        }}
                    />
                </View>
            </View>

            <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} numeric name="phoneNumber" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.mobileNumber} component={InputComponent} 
                returnKeyType="done"
                onSubmit={() => {
                    Keyboard.dismiss();
                }}
            />


            <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} email name="email" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.email} component={InputComponent} 
               returnKeyType="done"
               onSubmit={() => {
                   Keyboard.dismiss();
               }}
            />

            
            
              
            <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor}   name="address" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.address} component={InputComponent} 
                returnKeyType="done"
                onSubmit={() => {
                    Keyboard.dismiss();
                }}
            />
           
            <View style={{marginVertical:moderateScale(10), alignSelf:'center',justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                {this.renderSaveButtons()}
                {this.renderCloseButtons()}
            </View>
        </View>

    )
}
//{this.renderSignupButtons()}

    render(){
        const {currentUser} = this.props;
       
        return(
            <View style={{ flex:1 }}>
                <AppHeader navigator={this.props.navigator} title={Strings.updateProfile} showBack/> 
                <ScrollView style={{ flex:1 }} > 

                 <TouchableOpacity
                 onPress={()=>{
                    ImagePicker.openPicker({
                        includeExif: true,
                        forceJpg: true,
                      }).then(image => {
                        this.setState({ userImage: image.path });
                      });
                }}
                  style={{marginTop:moderateScale(8), alignSelf:'center'}}>
                    <Thumbnail
                     large
                     source={this.state.userImage?{uri:this.state.userImage}:currentUser.user.img?{uri:currentUser.user.img}:this.state.userImage!=null?{uri:this.state.userImage}:require('../assets/imgs/profileicon.png')}
                    />
                 </TouchableOpacity> 

                <View style={{justifyContent:'center',alignItems:'center', flex:1, marginTop:moderateScale(10),width:responsiveWidth(100)}}>
                    <View style={{width:responsiveWidth(85)}}>
                        {this.renderContent()}
                    </View>
                </View>    
                </ScrollView>
                {this.state.updateLoading && <LoadingDialogOverlay title={Strings.waitUpdateProfile}/>}

                {/* for city and area */}
                <SnackBar
                visible={this.state.noConnection!=null?true:false} 
                textMessage={this.state.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />

            </View>
            
        );
    }
}



const form = reduxForm({
    form: "UpdateProfile",
    enableReinitialize:true,
    validate,
})(UpdateProfile)

const mapDispatchToProps = {
    signup,
    getUser,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    signupLoading: state.signup.signupLoading,
    clientType: state.signup.clientType,
    errorText: state.signup.errorText,
    userToken: state.auth.userToken,
    currentUser: state.auth.currentUser,
    initialValues: {
        firstName: state.auth.currentUser.user.firstname,
        lastName: state.auth.currentUser.user.lastname,
        email: state.auth.currentUser.user.email,
        phoneNumber: state.auth.currentUser.user.phone,
        address: state.auth.currentUser.user.address
      },
    
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

