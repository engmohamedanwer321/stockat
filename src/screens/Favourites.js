import React,{Component} from 'react';
import {View,RefreshControl,NetInfo,StyleSheet} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import ProductCard from '../components/ProductCard';
import {
    RecyclerListView,
    LayoutProvider,
  } from 'recyclerlistview';
import AppSpinner from '../common/AppSpinner';
import {getFavouritesProducts} from '../actions/ProductAction'; 
import SnackBar from 'react-native-snackbar-component';
import ListFooter from '../components/ListFooter';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';



class Favourites extends Component {
    page = 1;
    

    state = {
        errorText:null
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }

    constructor(props) {
        super(props);    
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.props.getFavouritesProducts(this.props.currentUser.user.id,this.page,false);

            }else{
                this.setState({errorText:Strings.noConnection})
            }
          });
          
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(19);
          },
        );
      }

      componentDidMount(){
        this.enableDrawer()
       
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.props.getFavouritesProducts(this.props.currentUser.user.id,this.page,true);
                }
            }
          );
        
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }

    renderRow = (type, data, row) => {
        var arabic = /[\u0600-\u06FF]/;
     return (
         <View style={{ marginTop: moderateScale(3), justifyContent: 'center', alignItems: 'center' }}>
             <ProductCard
             data={data}
             onPress={()=>{
                 this.props.navigator.push({
                     screen:'ProductDetails',
                     animated: true,
                     passProps: {data:data}
                 })
             }}
             />
         </View>
     );
    }

    
    renderFooter = () => {
        return (
          this.props.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
             <ListFooter/>
            </View>
            : null
        )
      }
    

    render(){
        const {navigator} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={navigator} showBurger  title={Strings.favorites} />
                 <RecyclerListView                 
                    layoutProvider={this.renderLayoutProvider}
                    dataProvider={this.props.favProducts}
                    rowRenderer={this.renderRow}
                    renderFooter={this.renderFooter}
                    onEndReached={() => {
                        if (this.page <= this.props.pages) {
                            console.log('kak')
                            this.page+=1;
                            this.props.getFavouritesProducts(this.props.currentUser.user.id,this.page,false);
                        }
                        //this.props.getNotifications(this.page++, false,this.props.currentUser.token);
    
                    }}
                    refreshControl={
                    <RefreshControl colors={["#B7ED03"]}
                        refreshing={this.props.refresh}
                        onRefresh={() => {
                            this.page = 1
                            this.props.getFavouritesProducts(this.props.currentUser.user.id,this.page,true);
                        }
                        }
                    />
                    }
                    onEndReachedThreshold={.5}
                />
                

                {this.props.logoutLoading&&<LoadingDialogOverlay title={Strings.waitLogout}/>}
                <SnackBar
                    visible={this.props.errorText != null ? true : false||this.state.errorText != null ? true : false}
                    textMessage={Strings.noConnection}
                    messageColor='white'
                    backgroundColor={colors.primaryColor}
                    autoHidingTime={5000}
                />
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser : state.auth.currentUser,
    errorText: state.product.errorText,
    loading: state.product.loading,
    favProducts: state.product.favProducts,
    pages: state.product.pages,
    refresh: state.product.refresh,
    
});

const mapDispatchToProps = {
    getFavouritesProducts,
}

export default connect(mapStateToProps,mapDispatchToProps)(Favourites);
