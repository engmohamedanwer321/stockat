import React,{Component} from 'react';
import {
     View,Image
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import strings from '../assets/strings';






class About extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
  
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   
    componentDidMount(){
        this.enableDrawer();
    }




    render(){
        const {isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                
                <AppHeader title={strings.aboutUS}   showBurger navigator={this.props.navigator} />
                
                <Image style={{alignSelf:'center',marginTop:moderateScale(30), width:responsiveWidth(80),height:responsiveHeight(10)}} source={require('../assets/imgs/bluelogo.png')} />
                
                <View style={{alignSelf:'center'}}>
                    <AppText color='gray' text={strings.version} />
                </View>
               
                <View style={{borderWidth:1, marginHorizontal:moderateScale(4), alignSelf:isRTL?'flex-end':'flex-start', marginTop:moderateScale(30)}}>
                    <View style={{alignSelf:isRTL?'flex-end':'flex-start'}}>
                    <AppText color={colors.buttonColor} fontWeight='500' text={strings.appName} fontSize={responsiveFontSize(2.5)} />
                    </View>
                        
                    <AppText color='gray'  text={strings.aboutTitle} fontSize={responsiveFontSize(2.2)} />
                </View>
               
                <View style={{backgroundColor:'#f2efef', flexDirection:isRTL?'row-reverse':'row', width:responsiveWidth(100),height:responsiveHeight(7),justifyContent:'center',alignItems:'center',position:'absolute',bottom:0,right:0,left:0}}>
                    <AppText color='black'  text={strings.thisAppDesigned} fontSize={responsiveFontSize(2.2)} />
                    <AppText color={colors.buttonColor} fontWeight='600'  text={strings.firmName} fontSize={responsiveFontSize(2.5)} />
                    <Image style={{marginHorizontal:moderateScale(3), width:responsiveWidth(11),height:responsiveHeight(6)}} source={require('../assets/imgs/food1.jpg')} />
                </View>

            
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
})


export default connect(mapToStateProps)(About);