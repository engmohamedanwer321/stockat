
import React,{Component} from 'react';
import {View,NetInfo,ActivityIndicator,Alert} from 'react-native';
import { moderateScale, responsiveWidth,  responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Button,Picker,Item,Label} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {getOwnerProducts} from '../actions/ProductAction';
import { RNToasty } from 'react-native-toasty';
import SnackBar from 'react-native-snackbar-component';
import LoadingOverlay from '../components/LoadingOverlay';



class NotificationSearchedProductResponse extends Component {
   
    state = {
        searchLoading:false,
        searchData:null,

        ownerProductLoading:false,
        ownerProducts:[],
        onwerSelectedProduct:-1,

        noConnection:null,
        showContent:false,
        sendToAdminLoading:false,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }

    constructor(props){
        super(props);
        console.log('user token   '+this.props.currentUser.token);
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getSearchData()
                this.props.getOwnerProducts(this.props.currentUser.user.id) 
            }else{
                this.setState({noConnection:Strings.noConnection})
            }
          });
    }
    componentDidMount(){
        this.enableDrawer()
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})
                    this.getSearchData()  
                    this.props.getOwnerProducts(this.props.currentUser.user.id) 
            }
            }
          ); 
      
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    getSearchData = () => {
        this.setState({searchLoading:true})
        axios.get(`${BASE_END_POINT}/search/${this.props.subject}`)
        .then(response=>{
            console.log(response.data )
            this.setState({searchLoading:false, searchData:response.data });
        })
        .catch(error=>{
            console.log(error)
            this.setState({searchLoading:false});
            if(!error.response){
                this.setState({noConnection:Strings.noConnection});
            }
        })
    }

   /* getOwnerProducts = () => {
        this.setState({ownerProductLoading:true})
        axios.get(`${BASE_END_POINT}products/${this.props.currentUser.user.id}/owner`)
        .then(response=>{
            console.log("id    "+response.data.data[0].id )
            this.setState({onwerSelectedProduct:response.data.data[0].id, ownerProductLoading:false, ownerProducts:response.data.data });
        })
        .catch(error=>{
            console.log(error)
            this.setState({ownerProductLoading:false});
            if(!error.response){
                this.setState({noConnection:Strings.noConnection});
            }
        })
    }*/
    x=0;
    renderProductsPicker = () => {
        return(
            <Item style={{borderBottomColor:'#F2414E', marginTop:moderateScale(3), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
                <Label style={{fontSize:responsiveFontSize(2.4),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.selectProduct}</Label>
            </View>
             <View style={{flex:1}}>
                 
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.onwerSelectedProduct}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        
                        this.setState({onwerSelectedProduct:value});
                        console.log('my id   '+this.state.onwerSelectedProduct)
                     }}
                     >
                     
                      {this.props.ownerProducts.map(obj=>{
                          if(this.x==0){
                            this.setState({onwerSelectedProduct:obj.id})
                            this.x=1;
                          }
                          return(
                            <Picker.Item key={obj.id} label={obj.name} value={obj.id} />
                          )
                      })}
                    
                 </Picker>
                 
             </View>
         </Item>
        )
    }

    sendToAdmin = () => {
        this.setState({sendToAdminLoading:true});
        axios.put(`${BASE_END_POINT}search/${this.props.subject}/exist`, JSON.stringify({
            availableProduct:this.state.onwerSelectedProduct,
         }), {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`  
            },
          }).then(response=>{
              this.setState({sendToAdminLoading:false});
              RNToasty.Success({title:Strings.sendToAdmin});
             
          }).catch(error=>{
              console.log(error.response);
            this.setState({sendToAdminLoading:false});
              if(!error.response){
                
              }
          })
    }

    render(){
        const {navigator,isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.notificationDetails} showBack navigator={navigator}/>
                <View style={{ alignSelf:isRTL?'flex-end':'flex-start', marginTop:moderateScale(5), marginHorizontal:moderateScale(3)}}>
                    <AppText fontSize={responsiveFontSize(2.5)} color={colors.primaryColor} text={Strings.searchTitle} />
                    <AppText fontSize={responsiveFontSize(3.2)} color={colors.skipIconColor} text={this.state.searchData&&` " ${this.state.searchData.search} " `} />
                </View>

                 {
                 this.state.showContent&&
                 <View style={{position:'absolute',bottom:-12,left:0,right:0, marginBottom:moderateScale(15),justifyContent:'center',flexDirection:this.props.isRTL?'row-reverse':'row', height:responsiveHeight(20),backgroundColor:'#F7F7F7'}}>
                    <View style={{height:responsiveHeight(8)}}>
                         {this.renderProductsPicker()}
                    </View>

                 </View>
                 }

                <View style={{position:'absolute',bottom:0,left:0,right:0, marginTop:moderateScale(10)}}>
                   {this.state.showContent?
                   <View style={{width:responsiveWidth(100),flexDirection:isRTL?'row-reverse':'row'}}>
                         <Button
                            onPress={()=>{
                                if(this.state.onwerSelectedProduct==-1){
                                    RNToasty.Warn({title:Strings.selectProduct})
                                }else{
                                    this.sendToAdmin();
                                    this.setState({showContent:false})
                                }
                            }}
                            style={{width:responsiveWidth(50), backgroundColor:'#82C141', justifyContent:'center',alignItems:'center'}}>
                                <AppText fontSize={responsiveFontSize(2.5)} color='white' text={Strings.done}/>
                        </Button>
                        <Button

                            onPress={()=>{
                                
                                this.props.navigator.push({
                                    screen: 'OwnerAddNewProduct',
                                    animated: true,
                                    passProps:{refresh:true}
                                })
                                this.props.navigator.pop();
                            }}
                            style={{width:responsiveWidth(50), backgroundColor:'#F2414E', justifyContent:'center',alignItems:'center'}}>
                                <AppText fontSize={responsiveFontSize(2.5)} color='white' text={Strings.addProduct}/>
                        </Button>
                   </View>
                   :
                   <Button
                   onPress={()=>{this.setState({showContent:true})}}
                    style={{width:responsiveWidth(100), backgroundColor:'#82C141', justifyContent:'center',alignItems:'center'}}>
                       <AppText fontSize={responsiveFontSize(2.5)} color='white' text={Strings.exist}/>
                   </Button>
                }
                </View>
                {this.state.sendToAdminLoading&&<LoadingOverlay/>}
                <SnackBar
                visible={this.state.noConnection!=null?true:false} 
                textMessage={this.state.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser, 
    ownerProducts: state.product.ownerProducts,
})

const mapDispatchToProps = {
    getOwnerProducts,
}

export default connect(mapStateToProps,mapDispatchToProps)(NotificationSearchedProductResponse);