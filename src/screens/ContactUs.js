import React,{Component} from 'react';
import {
     View,ActivityIndicator,ScrollView,Keyboard
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Card,Button} from 'native-base';
import AppInput from '../common/AppInput';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import { RNToasty } from 'react-native-toasty';
import SnackBar from 'react-native-snackbar-component';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);



const validate = values => {
    const errors = {};

    const name = values.name
    const mobileNumber = values.mobileNumber;
    const email = values.email;
    const message = values.message;

    if (name == null) {
        errors.name = Strings.require;
    }
    if (mobileNumber == null) {
        errors.mobileNumber = Strings.require;
    }
    if (email == null) {
        errors.email = Strings.require;
    }
    if (message == null) {
        errors.message = Strings.require;
    }

    return errors;
};

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}




class ContactUs extends Component {

    state = {
        loading:false,
        errorText:null,
    }
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
  
    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   
    componentDidMount(){
        this.enableDrawer();
    }
    onSend(values) {
        this.setState({loading:true});
        axios.post(`${BASE_END_POINT}contact-us`, JSON.stringify({
            name: values.name,
            number: values.mobileNumber,
            email: values.email,
            message: values.message
          }), {
          headers: {
            'Content-Type': 'application/json',
            //'Authorization': `Bearer ${BE_token}`
          },
        }).then(response=>{
            this.setState({loading:false,errorText:null});
            console.log('contatct us send mail success')
            RNToasty.Success({title: Strings.sendSuccessfully})
        }).catch(error=>{
            this.setState({loading:false});
            if(!error.response){
                this.setState({errorText:Strings.noConnection});
            }
        })
    }

    renderSendButtons() {
        const { handleSubmit } = this.props;
        return (
            <MyButton
            onPress={!this.state.loading&&handleSubmit(this.onSend.bind(this))}
            style={{marginVertical:moderateScale(8), alignSelf:'center', justifyContent:'center',alignItems:'center',height:responsiveHeight(7),width:responsiveWidth(50),backgroundColor:colors.darkPrimaryColor,borderRadius:moderateScale(10)}}
            >
               {this.state.loading?
               <ActivityIndicator color='white'/>
               :
               <AppText fontSize={responsiveFontSize(3)} text={Strings.send} color='white' />
               }
            </MyButton>
        );
    }

    renderContent() {
        return (
            <View>
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} name="name" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.name} component={InputComponent}
                 returnKeyType="done"
                    onSubmit={() => {
                       Keyboard.dismiss()
                    }}
                />

                <Field borderColor='gray' textColor={colors.darkPrimaryColor} name="mobileNumber" isRTL={this.props.isRTL} numeric password={true} label={Strings.mobileNumber} component={InputComponent}
                    returnKeyType="done"
                    inputRef={el => this.passwordField = el }
                    onSubmit={() => {
                        Keyboard.dismiss()
                    }}
                />

                <Field borderColor='gray' textColor={colors.darkPrimaryColor} name="email" isRTL={this.props.isRTL} email  label={Strings.email} component={InputComponent}
                    returnKeyType="done"
                    inputRef={el => this.passwordField = el }
                    onSubmit={() => {
                        Keyboard.dismiss()
                    }}
                />

                <Field borderColor='gray' textColor={colors.darkPrimaryColor} name="message" isRTL={this.props.isRTL}  label={Strings.message} component={InputComponent}
                    returnKeyType="done"
                    inputRef={el => this.passwordField = el }
                    onSubmit={() => {
                        Keyboard.dismiss()
                    }}
                />
                
                {this.renderSendButtons()}
                           
            </View>

        )
    }

    renderNoConnection = () => {
        setTimeout(()=>{
            this.setState({errorText:null})
        },4000);
        return(
            <View style={{justifyContent:'center', elevation:10,zIndex:10000, backgroundColor:colors.darkPrimaryColor, position: 'absolute', bottom: 0, right: 0, left: 0, width: responsiveWidth(100), height: responsiveHeight(6)}}>
                <View style={{marginHorizontal:moderateScale(5)}}>
                    <AppText color='white' text={this.state.errorText} fontSize={responsiveFontSize(2.5)}  />
                </View>
            </View>
        )
    }

    render(){
        const {isRTL} = this.props;
        return(
            <View style={{flex:1}}>               
                <AppHeader title={Strings.contactUS}   showBurger navigator={this.props.navigator} />
                 <Card>
                     <View style={{ alignItems:'center',flexDirection:isRTL?'row-reverse':'row',marginTop:moderateScale(9),marginHorizontal:moderateScale(10)}}>
                        <Icon size={18} color={colors.buttonColor} name='phone' />
                        <AppText fontWeight='600' fontSize={responsiveFontSize(3)} text='  :  ' />
                        <AppText fontSize={responsiveFontSize(2.5)} text='01009028282 - 0224950783' />
                     </View>

                     <View style={{alignItems:'center',flexDirection:isRTL?'row-reverse':'row',marginTop:moderateScale(9),marginHorizontal:moderateScale(10)}}>
                        <Icon size={18} color={colors.buttonColor} name='envelope' />
                        <AppText fontWeight='600' fontSize={responsiveFontSize(3)} text='  :  ' />
                        <AppText fontSize={responsiveFontSize(2.5)} text='info@stockat.com - sales@stockat.com' />
                     </View>

                     <View style={{alignItems:'center',flexDirection:isRTL?'row-reverse':'row',marginVertical:moderateScale(9),marginHorizontal:moderateScale(10)}}>
                        <Icon size={18} color={colors.buttonColor} name='map-marker-alt' />
                        <AppText fontWeight='600' fontSize={responsiveFontSize(3)} text='  :  ' />
                        <AppText fontSize={responsiveFontSize(2.5)} text='01009028282 - 0224950783' />
                     </View>
                 </Card>        
                <ScrollView showsVerticalScrollIndicator={false}>
                <Card>
                    <View style={{marginVertical:moderateScale(4),marginHorizontal:moderateScale(5)}}>
                        {this.renderContent()}
                    </View>
                </Card>
                {
                    this.state.errorText!=null &&
                    this.renderNoConnection()
                }
            </ScrollView>
            </View>
        )
    }
}

const form = reduxForm({
    form: "ContactUs",
    validate,
})(ContactUs)

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
})


export default connect(mapToStateProps)(form);