import React,{Component} from 'react';
import {ActivityIndicator, View,Image,ScrollView,Alert} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import { Button, Icon,Item,Label,Picker } from 'native-base';

class Success extends Component{

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
    
    componentDidMount(){
        this.enableDrawer()
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    render(){
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={this.props.navigator} showBack title={Strings.success} />
                 <View style={{marginTop:moderateScale(20),width:responsiveWidth(100),justifyContent:'center',alignItems:'center'}}>
                    <Image
                    resizeMode='contain'
                    source={require('../assets/imgs/success.png')}
                    style={{width:responsiveWidth(25),height:responsiveHeight(14)}}
                    />
                    <AppText fontSize={responsiveFontSize(3)} text={Strings.productSendSuccessfully} color={colors.primaryColor} />
                    <View style={{width:responsiveWidth(70)}}>
                        <AppText textAlign='center' fontSize={responsiveFontSize(2)} text={Strings.review} color='#CCCCCC' />
                    </View>
                 </View>

                
            </View>
        )
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
})


export default connect(mapToStateProps)(Success);