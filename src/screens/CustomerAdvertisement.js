import React,{Component} from 'react';
import {
    ActivityIndicator, View,ImageBackground,TouchableOpacity,
    FlatList,ScrollView,Alert,NetInfo
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CompanyCard from '../components/CompanyCard';
import ProductCard from '../components/ProductCard';
import BrandCard from '../components/BrandCard';
import strings from '../assets/strings';
import { Button, Fab } from 'native-base';
import {getTopBrand} from '../actions/BrandAction';
import {getTopProducts} from '../actions/ProductAction';
import {getTopCompanies} from '../actions/CompanyAction';
import SnackBar from 'react-native-snackbar-component';
import ActionButton from 'react-native-action-button';
import AppSpinner from '../common/AppSpinner';
import ContentLoader, { Facebook,Code  } from 'react-content-loader'
import { RNToasty } from 'react-native-toasty';
import {AddProductToBacket} from '../actions/OrderAction';
import { BASE_END_POINT} from '../AppConfig';
import axios from 'axios';
//import Animation from 'lottie-react-native';
import LottieView from 'lottie-react-native';
import anim from '../assets/animations/preloader.json';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';




class CustomerAdvertisement extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    state = {
        fabActive: false,
        errorText:null,
        brandLoading:true,
        companyLoading:true,
        productLoading:true,
        topBrands:null,
        topProducts:null,
        topCompanies:null,
        flag:false,

    }

    getTopBrand = () => {
        
            this.setState({flag:true})
            axios.get(`${BASE_END_POINT}brands/topBrand`)
            .then(response=>{
                    this.setState({brandLoading:false, topBrands:response.data.data})
            }).catch(error=>{
                if (!error.response) {
                    this.setState({brandLoading:false, errorText:strings.noConnection})
                  }
            })
    }

     getTopProducts = () => {

         axios.get(`${BASE_END_POINT}products/topProduct`)
             .then(response => {
                 this.setState({productLoading:false, topProducts:response.data.data})
             }).catch(error => {
                 if (!error.response) {
                    this.setState({productLoading:false, errorText:strings.noConnection})
                 }
             })

    }

     getTopCompanies = () => {
            axios.get(`${BASE_END_POINT}companies/topCompany`)
            .then(response=>{
                console.log(response.data)
               this.setState({companyLoading:false, topCompanies:response.data.data})
            }).catch(error=>{
                if (!error.response) {
                    this.setState({companyLoading:false, errorText:strings.noConnection})                  }
            })
    }


    constructor(props){
        super(props);
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getTopBrand()
                this.getTopProducts()
                this.getTopCompanies()
            }else{
                this.setState({brandLoading:false, errorText:strings.noConnection})
            }
          });
    }

    componentDidMount(){
        this.enableDrawer()        
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getTopBrand()
                    this.getTopProducts()
                    this.getTopCompanies()
                }
            }
          );
         
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

   leftComponent = () => (
        <View style={{flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <TouchableOpacity
            onPress={()=>{
                if(this.props.currentUser){
                    this.props.navigator.push({
                        screen:'Notifications',
                        animated:true,
                      })
                }else{
                    RNToasty.Warn({title:strings.checkUser}) 
                }
            }}
             style={{marginHorizontal:moderateScale(7)}}>
                <Icon name='bell' color='white' size={20} />
            </TouchableOpacity>
            
        </View>
   )

    MyLoader = () => (
    <ContentLoader  height={140} speed={1} primaryColor={'#333'} secondaryColor={'#999'}>
      {/* Pure SVG */}
      <rect x="0" y="0" rx="5" ry="5" width="70" height="70" />
      <rect x="80" y="17" rx="4" ry="4" width="300" height="13" />
      <rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
    </ContentLoader>
  )

  loader =() => (
    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <ActivityIndicator  color='white'  />
    </View>

  )

  renderBrand = () => (
    <View style={{height:responsiveHeight(23),alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
        <View style={{marginHorizontal:moderateScale(5),marginVertical:moderateScale(3)}} >
            <AppText color='white' text={strings.topBrands} fontSize={responsiveFontSize(3)} fontWeight='600' />
        </View>
        <FlatList horizontal 
        showsHorizontalScrollIndicator={false}
        inverted={this.props.isRTL&&-1}
        data={this.state.topBrands}
        renderItem={({item}) => (
            <BrandCard image={item.img} /> 
        )}
        />
    </View>
  )

  renderProducts = (isRTL) => (
   <View style={{marginTop:moderateScale(1), height:responsiveHeight(40)}}>
        <View style={{ alignItems: 'center', marginHorizontal: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between' }}>
            <AppText color='white' text={strings.topProducts} fontSize={responsiveFontSize(3)} fontWeight='600' />
            <Button style={{ flexDirection: isRTL ? 'row-reverse' : 'row', backgroundColor: 'white', padding: moderateScale(3) }}>
                <Icon style={{ marginHorizontal: moderateScale(2) }} color={colors.skipIconColor} name='list' />
                <AppText color={colors.primaryColor} text={strings.topProducts} />
          </Button>
    </View>
      <FlatList horizontal
          showsHorizontalScrollIndicator={false}
          inverted={isRTL && -1}
          data={this.state.topProducts}
          renderItem={({ item }) => (
              <ProductCard
               onPress={()=>{
                   this.props.navigator.push({
                       screen:'ProductDetails',
                       animated:true,
                       passProps:{productData:item}
                   })
               }}
               buyProduct={()=>{
                this.props.AddProductToBacket({
                    product:item.id,
                    count:1
                },item)
                RNToasty.Success({title:Strings.addProductSuccessfuly})
              }} data={item} />
          )}
      /> 
   </View>
  )

  renderCompanies = (isRTL) => (
      <View style={{marginBottom:moderateScale(1),height:responsiveHeight(23),alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>
          <View style={{marginBottom:moderateScale(1), marginHorizontal: moderateScale(5) }} >
              <AppText color='white' text={strings.topCompanies} fontSize={responsiveFontSize(3)} fontWeight='600' />
          </View>
          <FlatList
           horizontal
              showsHorizontalScrollIndicator={false}
              inverted={isRTL && -1}
              data={this.state.topCompanies}
              renderItem={({ item }) => (
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <CompanyCard image={item.img} />
                      <View>
                          <AppText text={item.companyname} fontSize={responsiveFontSize(2.7)} color='white' />
                      </View>
                  </View>
              )}
          />
      </View>
  )

    render(){
         console.log('5ara')
        console.log(this.props.orders);
        const {isRTL,topBrands,topProduct,TopCompanies,navigator} = this.props;
        return(
           <ImageBackground
            source={require('../assets/imgs/boodyCarBackground2.png')}
             style={{flex:1}}>

                <AppHeader showBurger  navigator={this.props.navigator}  title={strings.advertisement} />
                
              {
                  this.state.brandLoading&&this.state.companyLoading&&this.state.brandLoading?
                    this.state.flag&&
                        <View style={{ color: 'rgba(0,0,0,1)', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <LottieView
                                source={anim}
                                autoPlay
                                loop
                            />
                           {/* <AppText color='white' text={strings.waitLoading} fontSize={responsiveFontSize(3)} /> */}
                        </View>
                    :
                        <View>
                            {this.renderBrand()}
                            {this.renderProducts(isRTL)}
                            {this.renderCompanies(isRTL)}
                            <ActionButton
                                onPress={() => {
                                    this.setState({ fabActive: true })
                                    flag = 1;
                                }}
                                active={this.state.fabActive}
                                hideShadow={true}
                                bgColor='rgba(0,0,0,0.6)'
                                position={isRTL ? 'left' : 'right'}
                                buttonColor={colors.skipIconColor}
                                renderIcon={() => <Icon color='white' size={20} name='search' />}
                            >

                                <ActionButton.Item

                                    textStyle={{ fontSize: responsiveFontSize(1.3) }}
                                    textContainerStyle={{ width: responsiveWidth(27), justifyContent: 'center', alignItems: 'center', borderRadius: moderateScale(3.5) }} size={moderateScale(15)} buttonColor={colors.skipIconColor} title={strings.advancedSearch}
                                    onPress={() => {
                                        this.setState({ fabActive: false })
                                        navigator.push({
                                            screen: 'ProductSearch',
                                            animated: true,
                                            passProps: { index: 0 }
                                        })
                                    }} >
                                    <AppText text='A' color='black' fontSize={responsiveFontSize(3)} />
                                </ActionButton.Item>

                                <ActionButton.Item onPress={() => {
                                    this.setState({ fabActive: false })
                                    navigator.push({
                                        screen: 'ProductSearch',
                                        animated: true,
                                        passProps: { index: 1 }
                                    })
                                }}
                                    textContainerStyle={{ width: responsiveWidth(27), justifyContent: 'center', alignItems: 'center', borderRadius: moderateScale(3.5) }} size={moderateScale(15)} buttonColor={colors.primaryColor} title={strings.normalSearch}
                                >
                                    <AppText text='N' color='white' fontSize={responsiveFontSize(3)} />
                                </ActionButton.Item>

                            </ActionButton>
                        </View>
            }
  
           
            {this.props.logoutLoading&&<LoadingDialogOverlay title={strings.waitLogout}/>}
            <SnackBar
                visible={this.props.errorText!=null?true:false||this.state.errorText!=null?true:false} 
                textMessage={strings.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
           </ImageBackground>
        )
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    //loading: state.auth.loading,
    topBrands: state.brand.topBrands,
    errorText: state.brand.errorText,
    brandLoading: state.brand.brandLoading,
    currentUser : state.auth.currentUser,
    topProduct: state.product.topProduct,
    productLoading : state.product.productLoading,
    TopCompanies: state.company.TopCompanies,
    companyLoading : state.company.companyLoading,
    orders: state.order.orders,
    logoutLoading: state.menu.logoutLoading,
    

})

const mapDispatchToProps = {
    getTopBrand,
    getTopProducts,
    getTopCompanies,
    AddProductToBacket
}

export default connect(mapToStateProps,mapDispatchToProps)(CustomerAdvertisement);
