import React, { Component } from 'react';
import {
  View,StatusBar,AsyncStorage,Keyboard,ScrollView,NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import { Button,  Icon, Item, Picker, Label } from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import AppSpinner from '../common/AppSpinner';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import Checkbox from 'react-native-custom-checkbox';
import { signup } from '../actions/SignupAction';
import {CheckLoginWhenOpen,userToken} from '../actions/AuthActions';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import SnackBar from 'react-native-snackbar-component';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay'
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);



const validate = values => {
    const errors = {};

    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }

    if (!values.firstName) {
        errors.firstName = Strings.require;
    } else if (!isNaN(values.firstName)) {
        errors.firstName = Strings.errorName;
    }

    if (!values.lastName) {
        errors.lastName = Strings.require;
    } else if (!isNaN(values.lastName)) {
        errors.lastName = Strings.errorName;
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = Strings.require;
    } else if (!values.phoneNumber.startsWith('05')) {
        errors.phoneNumber = Strings.errorStartPhone;
    }  else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = Strings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = Strings.errorPhoneFormat
    }
    
    return errors;
};

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class CustomerUpdateProfile extends Component {

    state = {
        citiesLoadig:true,
        cities:[],
        selectedCity:this.props.currentUser.user.city.id,
        areaLoading:true,
        areas:[],
        selectedArea:this.props.currentUser.user.area.id,
        noConnection:null,
        updateLoading:false,
        token:this.props.currentUser.token,
    }

     getCities = () => {
           axios.get(`${BASE_END_POINT}cities`)
           .then(response => {
                this.setState({
                   citiesLoadig:false, 
                   cities:response.data,
                   noConnection:null,
                   //selectedCity:response.data[0].id,
                },()=>this.getAreas(response.data[0].id))
                
           }).catch(error => {
            if (!error.response) {
               this.setState({noConnection:Strings.noConnection}) 
            } 
          });
    }

    getAreas = (cityID) => {
        this.setState({areas:[],areaLoading:true})
           axios.get(`${BASE_END_POINT}cities/${cityID}/areas`)
           .then(response => {
                this.setState({
                    areaLoading:false,
                    areas:response.data,
                    noConnection:null
                   // selectedArea:response.data[0].id
                })
           }).catch(error => {
            console.log(error.response);
            if (!error.response) {
              this.setState({noConnection:Strigs.noConnection})
            } 
          });
    }
    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator; 
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getCities()
            }else{
                this.setState({noConnection:Strings.noConnection})
            }
          });
    }
    
    componentDidMount(){    
        this.disableDrawer();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})
                    this.getCities()
                }
            }
          );
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    
    onUpload(values) {
        console.log('values');
        console.log(values);
        this.setState({updateLoading:true})
        axios.put(`${BASE_END_POINT}user/updateInfo`, JSON.stringify({
            firstname: values.firstName,
            lastname: values.lastName,
            phone: values.phoneNumber,
            email: values.email,
            city: this.state.selectedCity,
            area: this.state.selectedArea,
          }), {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(response=>{
            console.log('user')
            
            console.log(response.data)
            this.setState({updateLoading:false,noConnection:null});           
            const user = {...this.props.currentUser,user:{...response.data.user}}
            console.log('current user')
            console.log(user)
            this.props.CheckLoginWhenOpen(user)
            console.log('current user')
            console.log(user)
            AsyncStorage.setItem('@BoodyCarUser', JSON.stringify(user));
            this.props.navigator.push({
                screen: 'CustomerProfile',
                animated: true,
            })
        }).catch(error => {
            console.log('error');
            this.setState({updateLoading:false});
              console.log(error.response);
            if (!error.response) {
            this.setState({noConnection:Strings.noConnection});
            } 
          });
    }
    
    renderSaveButton() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={
                handleSubmit(this.onUpload.bind(this)) 
            }
            style={{marginTop:moderateScale(10),marginBottom:moderateScale(10), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:colors.primaryColor,borderRadius:moderateScale(2.5)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.save} color='white' />
            </MyButton>
        );
    }

    renderCityPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(7), marginBottom:moderateScale(5),width:responsiveWidth(80),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
             <Label  style={{fontSize:responsiveFontSize(3),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.city}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.citiesLoadig?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedCity}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedCity:value,});
                        this.getAreas(value);
                     }}
                     >
                     {this.state.cities.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.cityName} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    renderAreaPicker = () => {
        console.log("rtl  "+ this.props.isRTL);
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(7), marginBottom:moderateScale(5),width:responsiveWidth(80),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
               <View style={{ flex:1}}>
               <Label style={{fontSize:responsiveFontSize(3),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.area}</Label>
               </View>
                <View style={{flex:1}}>
                    {
                        this.state.areaLoading?
                        <AppSpinner isRTL={this.props.isRTL} />
                        :
                        <Picker           
                        mode="dropdown"
                        iosHeader="Select your SIM"
                        iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                        selectedValue={this.state.selectedArea}
                        style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                        onValueChange={(value,index)=>{
                           // this.props.setArea(value);
                           this.setState({selectedArea:value});
                        }}
                        >
                       {this.state.areas.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.areaName} value={obj.id} />
                     ))}
                    </Picker>
                    }
                </View>
            </Item>
        )
}
    
    
    renderContent() {
       // const { navigator, isRTL } = this.props;
        return (
            <View>

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} name="firstName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.firstName} component={InputComponent}
                 returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} name="lastName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.lastName} component={InputComponent} 
                    returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} email name="email" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.email} component={InputComponent} 
                   returnKeyType="done"
                   onSubmit={() => {
                       Keyboard.dismiss();
                   }}
                />

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} numeric name="phoneNumber" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.phoneNumer} component={InputComponent} 
                    returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />

                {this.renderCityPicker()}
                {this.renderAreaPicker()}
                {this.renderSaveButton()}
                
            </View>

        )
    }

    render(){
        return(
            <View style={{ flex:1 }}>
                <AppHeader navigator={this.props.navigator} title={Strings.updateProfile} showBack/> 
                <ScrollView style={{ flex:1 }} >   
                <View style={{justifyContent:'center',alignItems:'center', flex:1, marginTop:moderateScale(5),width:responsiveWidth(100)}}>
                    <View style={{width:responsiveWidth(80)}}>
                        {this.renderContent()}
                    </View>
                </View>    
                </ScrollView>
                {this.state.updateLoading && <LoadingDialogOverlay title={Strings.updateProfile}/>}

                {/* for city and area */}
                <SnackBar
                visible={this.state.noConnection!=null?true:false} 
                textMessage={this.state.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />

            </View>
            
        );
    }
}



const form = reduxForm({
    form: "CustomerUpdateProfile",
    enableReinitialize:true,
    validate,
})(CustomerUpdateProfile)

const mapDispatchToProps = {
    signup,
    CheckLoginWhenOpen,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    signupLoading: state.signup.signupLoading,
    clientType: state.signup.clientType,
    errorText: state.signup.errorText,
    userToken: state.auth.userToken,
    currentUser: state.auth.currentUser,
    initialValues: {
        firstName: state.auth.currentUser.user.firstname,
        lastName: state.auth.currentUser.user.lastname,
        email: state.auth.currentUser.user.email,
        phoneNumber: state.auth.currentUser.user.phone,
      },
    
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

