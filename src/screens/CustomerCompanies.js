import React,{Component} from 'react';
import {View,TextInput,RefreshControl,StyleSheet,NetInfo} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import AppText from '../common/AppText'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
  } from 'recyclerlistview';
import {getCompanies} from '../actions/CompanyAction';
import CompanyCard from '../components/CompanyCard';
import AppSpinner from '../common/AppSpinner';
import SnackBar from 'react-native-snackbar-component';
import ListFooter from '../components/ListFooter';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';


class CustomerCompanies extends Component {

    page = 1;
    list=[];
    state={
        companyName:'',
        errorText:null,
        companies2: new DataProvider(),
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 3,
          (type, dim) => {
            dim.width = responsiveWidth(33);
            dim.height = responsiveHeight(20);
          },
        );
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                if(this.props.companies._size==0){
                    this.props.getCompanies(this.page,false);
                }
            }else{
                this.setState({errorText:Strings.noConnection})
            }
          });
          
      }
    

    componentDidMount(){
        this.enableDrawer()
        
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.props.getCompanies(this.page,true);
                }
            }
          );
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }
    
    renderRow = (type, data, row) => {
        this.list = this.props.companies._data;
     return (
    <View style={{ marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <CompanyCard onPress={()=>{
            this.props.navigator.push({
                screen: 'CompanyProducts',
                animated:true,
                passProps: {
                    companyName:data.companyname,
                    companyID: data.id
                }
            })
        }} image={data.img} /> 
        <View style={{marginTop:moderateScale(0.5)}}>
            <AppText text={data.companyname} fontSize={responsiveFontSize(2.7)} color={colors.primaryColor} />
        </View>
    </View>
     );
    }

     renderFooter = () => {
        return (
          this.props.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

   
    


    render(){
         const {navigator,isRTL} = this.props;
        return(
            <View style={{backgroundColor:'#CCCCCC', flex:1}}>
                <AppHeader  navigator={navigator} title={Strings.company} showBurger />
                <View style={{marginBottom:moderateScale(5), height:responsiveHeight(8), marginTop:moderateScale(6), justifyContent:'center',alignItems:'center',width:responsiveWidth(100)}}>     
                    <View style={{borderColor:'#CCCCCC',elevation:2,shadowOffset:{width:1,height:2}, backgroundColor:'white',width:responsiveWidth(85),borderRadius:moderateScale(12)}}>
                        <TextInput
                        onChangeText={(name)=>{
                            this.list = this.props.companies._data.filter(val=>{
                                if(val.companyname.toLowerCase().includes(name.toLowerCase())||val.companyname.toUpperCase().includes(name.toUpperCase()))
                                    return val;
                            })
                            this.setState({
                                companyName:name,
                                companies2: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.list)
                            })
                        }}
                        underlineColorAndroid='transparent'
                        placeholder={Strings.searchBrand} 
                        style={{paddingHorizontal:moderateScale(10), width:responsiveWidth(85),textAlign:isRTL?'right':'left'}}
                        />
                    </View>
                </View>
                 {this.state.errorText==null&&
                <RecyclerListView            
                layoutProvider={this.renderLayoutProvider}
                dataProvider={this.state.companyName.length>0 ? this.state.companies2 : this.props.companies}
                rowRenderer={this.renderRow}
                renderFooter={this.renderFooter}
                onEndReached={() => {
                   
                    if(this.page <= this.props.pages){
                        this.page++;
                        this.props.getCompanies(this.page, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.props.refresh}
                    onRefresh={() => {
                      this.page = 1
                      this.props.getCompanies(this.page, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}
        
            /> 
                }
                    {this.props.logoutLoading&&<LoadingDialogOverlay title={Strings.waitLogout}/>}
                 <SnackBar
                visible={this.props.errorText!=null?true:false||this.state.errorText!=null?true:false} 
                textMessage={Strings.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    footerSpiner: {
        alignItems: 'center',
        alignSelf: "flex-end",
        flex: 1,
        width: '100%',
        marginVertical: 10,
        backgroundColor: colors.primaryColor
      }
    
})

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    companies: state.company.companies,
    loading: state.company.loading,
    refresh: state.company.refresh,
    pages: state.company.pages,
    errorText: state.company.errorText,
    
  logoutLoading: state.menu.logoutLoading,
})

const mapDispatchToProps = {
    getCompanies,
}


export default connect(mapToStateProps,mapDispatchToProps)(CustomerCompanies);