import React,{Component} from 'react';
import {View,StyleSheet,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import withPreventDoubleClick from './withPreventDoubleClick';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
const MyButton =  withPreventDoubleClick(Button);


 class HomeProductCard extends Component {
     state={
         basketColor:null,
         added: false,
     }

     makeProductSponserd = () => {
        axios.put(`${BASE_END_POINT}products/${this.props.data.id}/sponsered`, null, {
            headers: {
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
        .then(response=>{
            console.log('product sponserd')
            console.log(response.data);       
            this.setState({uploadProduct:false})
            RNToasty.Success({title:Strings.prodectSponserd})               
        }).catch(error=>{
            console.log(error);
            console.log(error.message);
            console.log(error.status);
            console.log(error.response);
            this.setState({uploadProduct:false})
        })      
        
     }
     
    render(){
        const {data,onPress,isRTL} = this.props;
        return(
            <MyButton 
            onPress={()=>{
                if(onPress){
                   onPress();
                }
            }}
            style={{ marginHorizontal:moderateScale(2), marginVertical:moderateScale(1), alignSelf:'center', backgroundColor:'transparent',  width:responsiveWidth(isRTL?78:80),height:responsiveHeight(17)}}
            >

            <View style={{alignItems:'center', width:responsiveWidth(77),height:responsiveHeight(20),flexDirection:isRTL?'row-reverse':'row'}}>
                <FastImage 
                style={{marginHorizontal:moderateScale(3), width:responsiveWidth(20),height:responsiveHeight(15)}}
                source={{uri:data.img[0]}}
                />
                <View style={{alignItems:'center'}}>
                    <View style={{ alignSelf:isRTL?'flex-end':'flex-start', width:responsiveWidth(isRTL?51:54),  justifyContent:'space-between', flexDirection:isRTL?'row-reverse':'row'}}>
                        <AppText fontWeight='300' text={Strings.productName} color='black' fontSize={responsiveFontSize(2.5)} />
                        <AppText fontWeight='300' text={data.name} color='red' fontSize={responsiveFontSize(2.3)} />
                    </View>

                    <View style={{alignSelf:isRTL?'flex-end':'flex-start', marginVertical:moderateScale(2), width:responsiveWidth(isRTL?51:54)}}>
                        <AppText fontWeight='300' text={data.description} color='black' fontSize={responsiveFontSize(2.2)} />       
                    </View>

                    {data.owner.id!=this.props.currentUser.user.id?
                    <View style={{width:responsiveWidth(isRTL?54:56)}}>
                        <View style={{alignSelf:isRTL?'flex-start':'flex-end'}} >
                            <Icon type='Foundation' name='star' style={{color:'red'}} />
                        </View>
                    </View>
                    :
                    <View style={{alignSelf:isRTL?'flex-end':'flex-start', justifyContent:'space-between', width:responsiveWidth(isRTL?54:56), flexDirection:isRTL?'row-reverse':'row'}}>
                        <Button
                        onPress={()=>{
                            this.makeProductSponserd()
                        }}
                         bordered style={{borderColor:colors.buttonColor, width:responsiveWidth(20),justifyContent:'center',alignItems:'center',height:responsiveHeight(4),borderRadius:moderateScale(10),backgroundColor:'transparent'}}>
                            <AppText text={Strings.sponser} />
                        </Button>
                        <Icon type='Foundation' name='star' style={{color:'red'}} />
                    </View>
                    }
                    
                </View>
            </View>

            </MyButton>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    ordersData: state.order.ordersData,
});

export default connect(mapStateToProps)(HomeProductCard);
