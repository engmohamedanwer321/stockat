import React,{Component} from 'react';
import {View,StyleSheet} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import FastImage from 'react-native-fast-image'


class BrandCard extends Component {
    render(){
        const {image} = this.props;
        return(
            <View style={styles.card}>
                <FastImage style={styles.img} source={{uri:image}} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        justifyContent:'center',
        alignItems:'center',
        height:responsiveHeight(16),
        width: responsiveWidth(25),
        borderRadius: moderateScale(1.5),
        backgroundColor:'white',
        elevation:2,
        shadowOffset:{width:1,height:2},
        marginHorizontal: moderateScale(5),
    },
    img: {
        width: responsiveWidth(18),
        height: responsiveHeight(11)
    }
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(BrandCard);
