import React,{Component} from 'react';
import {View,Alert,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppText from '../common/AppText';
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);




class NotificationCard extends Component {
    
    state = {
        ground: 0,
    }
    
    componentDidMount(){
        moment.locale(this.props.isRTL ? 'ar' : 'en');
    }

    readNotification = (notID) => {
          console.log(notID)
        axios.put(`${BASE_END_POINT}notif/${notID}/read`,{}, {
            headers: {
              'Content-Type': 'application/json',
              //this.props.currentUser.token
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
        }).then(Response=>{
            console.log(Response)
        }).catch(error=>{
            console.log(error.response)
        })
    }
    render(){
        const {noti,isRTL,navigator} = this.props;
        //const dateToFormat = () => <Moment date={date} />,
        return(
            <MyTouchableOpacity style={{
             alignSelf:'center',      
            width: responsiveWidth(98),
            backgroundColor:noti.read||this.state.ground>0? 'rgba(250,250,250,0.8)' : '#E8ECFF',
            elevation:2,
            shadowOffset:{width:1,height:2},
            }}
            onPress={()=>{
                this.setState({ground:1});
               // this.readNotification(notID)
                /*if(                   
                    notDescription.toLowerCase().includes('new order'.toLocaleLowerCase())
                    //notDescription.toLowerCase().includes('reply on your search order')
                    ){
                    navigator.push({
                        screen: 'NotificationOrderDetails',
                        animated: true,
                        passProps:{
                            source: 'orders',
                            subject: subject,
                        }
                    })
                    
                }else if(             
                    notDescription.toLowerCase().includes('accept your Product'.toLocaleLowerCase())|| 
                    notDescription.toLowerCase().includes('Product Top'.toLocaleLowerCase())||
                    notDescription.toLowerCase().includes('Product Low'.toLocaleLowerCase())
                    ){
                    navigator.push({
                        screen: 'NotificationProductDetails',
                        animated: true,
                        passProps:{
                            source: 'orders',
                            subject: subject,
                        }
                    })
                }else if(notDescription.toLowerCase().includes('accept your rigister'.toLocaleLowerCase())){
                    navigator.push({
                        screen: 'Accept',
                        animated: true,
                    })
                }else if(notDescription.toLowerCase().includes('someone search about this product'.toLocaleLowerCase())){
                    navigator.push({
                        screen: 'NotificationSearchedProductResponse',
                        animated: true,
                        passProps:{
                            subject: subject,
                        }
                    })
                }*/               
            }}
            >
               <View style={{alignItems:'center',marginHorizontal:moderateScale(6), marginTop:moderateScale(4), flexDirection:isRTL?'row-reverse':'row'}}>
                    <Thumbnail small source={noti.target[0].img?{uri:noti.target[0].img}:require('../assets/imgs/profileicon.png')} />
                    <View style={{marginHorizontal:moderateScale(5)}}>
                        <AppText text={noti.description} fontSize={responsiveFontSize(2.4)} color={colors.buttonColor} />
                    </View>
               </View>
               
               <View style={{marginHorizontal:moderateScale(5), alignSelf:isRTL?'flex-start':'flex-end'}}>
                    <AppText text={moment(noti.createdAt).fromNow()} color='gray' />
               </View>

            </MyTouchableOpacity>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});



export default connect(mapStateToProps)(NotificationCard);
