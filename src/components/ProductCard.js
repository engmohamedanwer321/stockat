import React,{Component} from 'react';
import {View,StyleSheet,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
//import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import {Button,Icon} from 'native-base';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {RNToasty} from 'react-native-toasty';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class ProductCard extends Component {
     state={
         basketColor:null,
         added: false,
     }

     makeProductSponserd = () => {
        console.log("my product  "+this.props.data.product.id)
        axios.put(`${BASE_END_POINT}products/${this.props.data.product.id}/sponsered`, null, {
            headers: {
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
        .then(response=>{
            console.log('product sponserd')
            console.log(response.data);       
            this.setState({uploadProduct:false})
            RNToasty.Success({title:Strings.prodectSponserd})               
        }).catch(error=>{
            console.log(error);
            console.log(error.message);
            console.log(error.status);
            console.log(error.response);
            this.setState({uploadProduct:false})
        })      
        
     }

    render(){
        const {data,onPress,isRTL} = this.props;
        
        return(
            <MyButton 
            onPress={()=>{
                if(onPress){
                   onPress();
                }
            }}
            style={{marginVertical:moderateScale(4),marginBottom:15, alignSelf:'center', backgroundColor:'transparent', border:1, width:responsiveWidth(95),height:responsiveHeight(17)}}
            >

            <View style={{alignItems:'center', width:responsiveWidth(95),height:responsiveHeight(20),flexDirection:isRTL?'row-reverse':'row'}}>
                <FastImage 
                style={{marginHorizontal:moderateScale(3), width:responsiveWidth(25),height:responsiveHeight(15)}}
                source={{uri:data.product.img[0]}}
                />
                <View>
                    <View style={{justifyContent:'space-between', width:responsiveWidth(62), flexDirection:isRTL?'row-reverse':'row'}}>
                        <AppText fontWeight='300' text={Strings.productName} color='black' fontSize={responsiveFontSize(2.5)} />
                        <AppText fontWeight='300' text={data.product.name} color='red' fontSize={responsiveFontSize(2.3)} />
                    </View>

                    <View style={{marginVertical:moderateScale(2), width:responsiveWidth(62)}}>
                        <AppText fontWeight='300' text={data.product.description} color='black' fontSize={responsiveFontSize(2.2)} />       
                    </View>

                    {data.user.id!=this.props.currentUser.user.id?
                    <View style={{width:responsiveWidth(62)}}>
                        <View style={{alignSelf:isRTL?'flex-start':'flex-end'}} >
                            <Icon type='Foundation' name='star' style={{color:'red'}} />
                        </View>
                    </View>
                    :
                    <View style={{justifyContent:'space-between', width:responsiveWidth(62), flexDirection:isRTL?'row-reverse':'row'}}>
                        <Button
                        onPress={()=>{
                            this.makeProductSponserd()
                        }}
                         style={{width:responsiveWidth(20),justifyContent:'center',alignItems:'center',height:responsiveHeight(4),borderRadius:moderateScale(10),backgroundColor:'transparent',borderBottomColor:colors.darkPrimaryColor,borderWidth:1}}>
                            <AppText text={Strings.sponser} />
                        </Button>
                        <Icon type='Foundation' name='star' style={{color:'red'}} />
                    </View>
                    }
                </View>
            </View>

            </MyButton>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
    currentUser: state.auth.currentUser,
});

export default connect(mapStateToProps)(ProductCard);
