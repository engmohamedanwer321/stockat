import React,{Component} from 'react';
import {View,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppText from '../common/AppText';
import moment from 'moment'
import "moment/locale/ar"



class AkfaNotificationCard extends Component {
    render(){
        const {data,isRTL} = this.props;
        return(
            <TouchableOpacity style={{width:responsiveWidth(100),backgroundColor:'rgba(6, 19, 76, 0.08)'}}>
                <View style={{margin:moderateScale(2),flexDirection:isRTL?'row-reverse':'row',alignSelf:isRTL?"flex-end":'flex-start'}}>
                    <AppText text='Tasks Send' color='black' fontSize={responsiveFontSize(2.5)}/>
                    <AppText text='  " pla pla pla pla "' color='#06134C' fontSize={responsiveFontSize(2.5)}/>
                </View>

                <View style={{justifyContent:'space-between',margin:moderateScale(2),flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(95),alignSelf:'center'}}>
                    <View style={{flexDirection:isRTL?'row-reverse':'row'}}>
                        <AppText text='Follower : ' color='#06134C' fontSize={responsiveFontSize(2)}/>
                        <AppText text='Mohamed Anwer' color='#FF9900' fontSize={responsiveFontSize(2)}/>
                    </View>
                    <AppText text={moment('2018-02-08T00:00:00.000Z').fromNow()} color='#626262' fontSize={responsiveFontSize(2)}/>
                </View>
            </TouchableOpacity>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(AkfaNotificationCard);
