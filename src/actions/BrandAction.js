import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT} from '../AppConfig';
import {GET_TOP_BRANDS,GET_TOP_BRANDS_SUCCESS,GET_TOP_BRANDS_FAILD} from './types';


export function getTopBrand() {
    return dispatch => {
        dispatch({type:GET_TOP_BRANDS});
        axios.get(`${BASE_END_POINT}brands/topBrand`)
        .then(response=>{
            dispatch({type:GET_TOP_BRANDS_SUCCESS,payload:response.data.data})
        }).catch(error=>{
            if (!error.response) {
                dispatch({type:GET_TOP_BRANDS_FAILD,payload:Strigs.noConnection})
              }
        })
    }
}