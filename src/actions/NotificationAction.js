import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT} from '../AppConfig';
import {
    FETCH_NOTIFICATIONS_REFRESH,FETCH_NOTIFICATIONS_REQUEST,
    FETCH_NOTIFICATIONS_SUCCESS,FETCH_NOTIFICATIONS_FAIL
} from './types';
import { RNToasty } from 'react-native-toasty';



export function getNotifications(page,refresh,token) {
    return dispatch => {
        let uri=`${BASE_END_POINT}notif?page=${page}&limit=10`
        if(refresh){
            dispatch({type:FETCH_NOTIFICATIONS_REFRESH});
        }else{
            dispatch({type:FETCH_NOTIFICATIONS_REQUEST});
        }
        axios.get(uri, {
            headers: {
                //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0OSIsImlzcyI6ImJvb2RyQ2FyIiwiaWF0IjoxNTQyNzI1MTIyNTY1LCJleHAiOjE1NDI3MjU3MjczNjV9.iYKdxuK0fpf0ZcQQV77rjhZ4VCZ2O0TaRsisSh5RoAA
                //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMCIsImlzcyI6ImJvb2RyQ2FyIiwiaWF0IjoxNTQwNjUxMjk1NTUzLCJleHAiOjE1NDA2NTE5MDAzNTN9.vf2XkQfewGwBOyU4d-Yo7K6nu2xoVmHUDh-Xl5_DCgc
              'Content-Type': 'application/json',
              //token
              'Authorization': `Bearer ${token}`  
            },
        })
        .then(response=>{
            console.log('44444')
            console.log(response.data);
            if(page==1){
                if(response.data.data.length==0){
                    RNToasty.Info({title:Strings.notNotificatios})
                }
            }
            dispatch({type:FETCH_NOTIFICATIONS_SUCCESS,payload:response.data.data,pages:response.data.pageCount})
        }).catch(error=>{
            if (!error.response) {
                dispatch({type:FETCH_NOTIFICATIONS_FAIL,payload:Strigs.noConnection})
              }
        })
    }
}



