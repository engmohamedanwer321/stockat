import axios from 'axios';
import {AsyncStorage} from 'react-native';
import { 
    GET_ALL_CITIES,GET_DETERMINED_AREAS,LOAD_AREA,CLIENT_TYPE,SIGNUP_REQUEST,
    SIGNUP_SUCCESS,SIGNUP_FAIL,SIGNUP_STEP1,SIGNUP_STEP2,SIGNUP_STEP3,SIGNUP_STEP4,
    SELECT_COMPANY,GO_BACK,GO_NEXT,GO_ROOT
 } from './types';
import { GET_CITIES,SIGNUP,BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import Strigs from '../assets/strings';
import {Alert} from 'react-native';

export function clientType(type){
    return dispatch => {
        dispatch({type: CLIENT_TYPE, payload: type })
    }
}

export function signup(user,navigator) {
        return (dispatch,getState) => {       
          dispatch({ type: SIGNUP_REQUEST });
          console.log('my data   ');
          console.log(user);

          axios.post(`${BASE_END_POINT}signup`, JSON.stringify(user), {
            headers: {
              'Content-Type': 'application/json',
            },
          }).then(response => {
            console.log(response.data);
              console.log('done');
            AsyncStorage.setItem('@BoodyCarUser', JSON.stringify(response.data)); 
            dispatch({ type: SIGNUP_SUCCESS, payload: response.data });  
            navigator.resetTo({
              screen: 'Home',
              animated: true,
            });
            
          })
            .catch(error => {
                dispatch({type: SIGNUP_FAIL});
                console.log(error)
              if(getState().auth.user){
                return
              }
              console.log(error.response);
              if (!error.response) {
                dispatch({type:SIGNUP_FAIL,payload:Strigs.noConnection})
              }else if (error.response.status == 422) {
                dispatch({type: SIGNUP_FAIL, payload: Strigs.signupFail})
              }else{
                dispatch({type: SIGNUP_FAIL, payload: Strigs.loginError})
              }
            });
        };
}


export function goToBack(){
  return dispatch => {
    dispatch({type:GO_BACK})
  }
}

export function goToNext(){
  return dispatch => {
    dispatch({type:GO_NEXT})
  }
}

export function goToRoot(){
  return dispatch => {
    dispatch({type:GO_ROOT})
  }
}

     