import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT} from '../AppConfig';
import {
    GET_COMPANY,GET_COMPANY_SUCCESS,GET_COMPANY_FAILD,  
    FETCH_COMANY_REFRESH,FETCH_COMANY_REQUEST,
    FETCH_COMANY_SUCCESS,FETCH_COMANY_FAIL,
    GET_OWNER_COMPANY_FAILD,GET_OWNER_COMPANY_SUCCESS,GET_OWNER_COMPANY

} from './types';


export function getCompanies(page,refresh) {
    return dispatch => {
        let uri=`${BASE_END_POINT}companies?page=${page}&limit=10`
        if(refresh){
            dispatch({type:FETCH_COMANY_REFRESH});
        }else{
            dispatch({type:FETCH_COMANY_REQUEST});
        }
        axios.get(uri)
        .then(response=>{
            console.log('44444')
            console.log(response.data);
            dispatch({type:FETCH_COMANY_SUCCESS,payload:response.data.data,pages:response.data.pageCount})
        }).catch(error=>{
            if (!error.response) {
                dispatch({type:FETCH_COMANY_FAIL,payload:Strigs.noConnection})
              }
        })
    }
}

export function getTopCompanies() {
    return dispatch => {
        dispatch({type:GET_COMPANY});
        axios.get(`${BASE_END_POINT}companies/topCompany`)
        .then(response=>{
            console.log(response.data)
            dispatch({type:GET_COMPANY_SUCCESS,payload:response.data.data})
        }).catch(error=>{
            if (!error.response) {
                dispatch({type:GET_COMPANY_FAILD,payload:Strigs.noConnection})
              }
        })
    }
}

export function getOwnerCompanies() {
    return dispatch => {
        dispatch({type:GET_OWNER_COMPANY});
        axios.get(`${BASE_END_POINT}companies`)
        .then(response=>{
            console.log(response.data)
            dispatch({type:GET_OWNER_COMPANY_SUCCESS,payload:response.data.data})
        }).catch(error=>{
            if (!error.response) {
                dispatch({type:GET_OWNER_COMPANY_FAILD,payload:Strigs.noConnection})
              }
        })
    }
}