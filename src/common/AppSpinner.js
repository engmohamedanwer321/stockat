import React, { Component } from 'react';
import { Spinner } from "native-base" ;
import * as colors from '../assets/colors'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";

const AppSpinner = (isRTL) => <Spinner style={{alignSelf:isRTL? 'flex-start':'flex-end', height:responsiveHeight(2)}} size='small' color={colors.primaryColor} />

export default AppSpinner ;