package com.stockat;

import com.evollu.react.fcm.FIRMessagingPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import ui.toasty.RNToastyPackage;
import org.wonday.pdf.RCTPdfView;
import com.reactnativenavigation.NavigationApplication;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.reactnativenavigation.bridge.NavigationReactPackage;
import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.airbnb.android.react.maps.MapsPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication {


  @Override
  public boolean isDebug() {
    // Make sure you are using BuildConfig from your own application
    return BuildConfig.DEBUG;
  }

  protected List<ReactPackage> getPackages() {
    // Add additional packages you require here
    // No need to add RnnPackage and MainReactPackage
    return Arrays.<ReactPackage>asList( // <== this
            new MainReactPackage(),
            new PickerPackage(),
            new RNGestureHandlerPackage(),
            new LottiePackage(),
            new ReactNativeLocalizationPackage(),
            new RNFetchBlobPackage(),
            new VectorIconsPackage(),
            new RNToastyPackage(),
            new RCTPdfView(),
            new NavigationReactPackage(),
            new RNFusedLocationPackage(),
            new FastImageViewPackage(),
            new MapsPackage(),
            new LocationServicesDialogBoxPackage(),
            new FIRMessagingPackage()
    );
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }
  @Override
  public String getJSMainModuleName() {
    return "index";
  }

 /* @Override
  public void onCreate() { // <-- Check this block exists
    super.onCreate();
    SoLoader.init(this,  false); // <-- Check this line exists within the block
  }*/

}