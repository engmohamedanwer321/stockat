package com.stockat;

import android.content.Intent;


import com.google.firebase.FirebaseApp;
import com.reactnativenavigation.controllers.SplashActivity;

public class MainActivity extends SplashActivity {

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        try {
            FirebaseApp.initializeApp(this);
        }
        catch (Exception e) {
        }
    }

//    @Override
//    public LinearLayout createSplashLayout() {
//        LinearLayout linearLayout = new LinearLayout(this);
//        Display display = getWindowManager().getDefaultDisplay();
//        int width=display.getWidth();
//        int height=display.getHeight();
//        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        ImageView imageView = new ImageView(this);
//        imageView.setImageResource(R.mipmap.splash);
//        imageView.setLayoutParams(lp);
//        linearLayout.setLayoutParams(lp);
//        linearLayout.setGravity(Gravity.CENTER | Gravity.BOTTOM);
//        getWindow().setFlags(
//                WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN
//        );
//        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
//        linearLayout.addView(imageView);
//        return linearLayout;
//    }
}